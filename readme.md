## Installation

composer install

### Generate the SSH keys:

- openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
- openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
- in .env change JWT_PASSPHRASE to your passphrase 

### Settings

set your APP_URL in .env

Change database connection in DATABASE_URL

### Tests

Configure .env.test

cp env.test.example .env.test

Change database connection in DATABASE_URL in .env.test (use your test database)

Run tests: php vendor/bin/codecept run
