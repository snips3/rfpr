#!/bin/bash
PROJECT_NAME="$1"
BRANCH="$2"
JOB_TOKEN="$3"
REGISTRY="$4"

echo "project $PROJECT_NAME"
echo "branch $BRANCH"
cd "$HOME/$PROJECT_NAME" || exit 1
docker login -u gitlab-ci-token -p "${JOB_TOKEN}" "${REGISTRY}" || exit 1
docker-compose pull || exit 1
docker-compose down --volumes || exit 1
docker-compose up -d || exit 1
# bug
# https://github.com/docker/compose/issues/5696
export COMPOSE_INTERACTIVE_NO_CLI=1
docker-compose exec -T -u www-data rich-family-backend-php sh -c "mkdir -p config/jwt" || exit 1
docker-compose exec -T -u www-data rich-family-backend-php sh -c "(echo 05cb0134f6fe3942860a2d8132a5ac43; echo 05cb0134f6fe3942860a2d8132a5ac43) | openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096" || exit 1
docker-compose exec -T -u www-data rich-family-backend-php sh -c "echo 05cb0134f6fe3942860a2d8132a5ac43 | openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout" || exit 1
docker-compose exec -T -u www-data rich-family-backend-php sh -c "chmod 644 config/jwt/public.pem config/jwt/private.pem" || exit 1

docker-compose exec -T -u www-data rich-family-backend-php sh -c "php bin/console d:s:u -f" || exit 1
#docker-compose exec -T -u www-data rich-family-backend-php sh -c "php bin/console cache:generate" || exit 1

