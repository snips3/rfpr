to build localy you must be in root directory (where are is .git)
run in order
```
docker-compose -f docker-compose-local.yml build --no-cache rich-family-backend-build-php 
docker-compose -f docker-compose-local.yml build --no-cache rich-family-backend-build-stage
docker-compose -f docker-compose-local.yml up -d --scale rich-family-backend-build-php=0 --scale rich-family-backend-build-stage=0
```
run as needed
```
docker-compose -f docker-compose-local.yml exec rich-family-backend-php sh -c "php bin/console d:s:u -f"
docker-compose -f docker-compose-local.yml exec rich-family-backend-php sh -c "php bin/console cache:generate" 
```

to stop and remove containers use
```
docker-compose -f docker-compose-local.yml down --volumes
```



