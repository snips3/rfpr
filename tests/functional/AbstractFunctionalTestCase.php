<?php

namespace App\Tests\Functional;

use App\Domain\Customer\Entity\Customer;
use App\Kernel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Gedmo\Tree\RepositoryInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class AbstractFunctionalTestCase extends WebTestCase
{
    private EntityManagerInterface $entityManager;
    private ORMExecutor $executor;
    protected KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        // Configure variables
        $this->entityManager = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->executor = new ORMExecutor($this->entityManager, new ORMPurger());

        // Run the schema update tool using our entity metadata
        $schemaTool = new SchemaTool($this->entityManager);
        $schemaTool->updateSchema($this->entityManager->getMetadataFactory()->getAllMetadata());
    }

    protected function loadFixture(Fixture $fixture)
    {
        $loader = new Loader();
        $fixtures = is_array($fixture) ? $fixture : [$fixture];
        foreach ($fixtures as $item) {
            $loader->addFixture($item);
        }

        $this->executor->execute($loader->getFixtures());
    }

    protected static function getKernelClass(): string
    {
        return Kernel::class;
    }

    protected function getUser(): Customer
    {
        $customers = $this->entityManager->getRepository(Customer::class)->findall();

        return $customers[array_rand($customers)];
    }

    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    protected function getRepository(string $repositoryClassname): ServiceEntityRepository
    {
        return $this->entityManager->getRepository($repositoryClassname);
    }

    protected function getClass(string $containerName): object
    {
        return self::$kernel->getContainer()->get($containerName);
    }

    protected function getKernelClient(): KernelBrowser
    {
        return $this->client;
    }
}
