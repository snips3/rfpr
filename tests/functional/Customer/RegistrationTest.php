<?php

namespace App\Tests\functional\Customer;

use App\DataFixtures\CustomerFixtures;
use App\Domain\Customer\Entity\Customer;
use App\Domain\Customer\ParamConverter\CustomerPhoneParamConverter;
use App\Domain\Customer\Repository\CustomerRepository;
use App\Utility\Phone\Phone;
use App\Utility\Phone\PhoneException;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationSuccessResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Tests\functional\AbstractFunctionalTestCase;

class RegistrationTest extends AbstractFunctionalTestCase
{
    public function testConvertPayload()
    {
        $customerFixture = new CustomerFixtures();
        $this->loadFixture($customerFixture);
        $customer = $customerFixture->getReference('customer_phone');

        $payload = json_encode(['phone' => $customer->getPhone()->getValue()]);

        $request = $this->mockRequest($payload);
        $customerRepository = $this->mockCustomerRepository($customer);

        $paramConverterConfig = $this->mockParamConverter();
        $customerPhoneParamConverter = new CustomerPhoneParamConverter($customerRepository);

        $customerPhoneParamConverter->apply($request, $paramConverterConfig);

        $requestCustomer = $request->attributes->get('customer');

        $this->assertEquals($customer, $requestCustomer);
    }

    public function testConvertPhoneInvalid()
    {
        $customerFixture = new CustomerFixtures();
        $this->loadFixture($customerFixture);
        $customer = $customerFixture->getReference('customer_phone');

        $payload = '{"phone":"abc"}';

        $request = $this->mockRequest($payload);
        $customerRepository = $this->mockCustomerRepository($customer);

        $paramConverterConfig = $this->mockParamConverter();
        $customerPhoneParamConverter = new CustomerPhoneParamConverter($customerRepository);

        $this->expectException(PhoneException::class);

        $customerPhoneParamConverter->apply($request, $paramConverterConfig);
    }

    public function testAuthWithoutPhone()
    {
        $client = $this->getKernelClient();

        $payload = '{"phone":"79800000000"}';
        $phone = '79800000000';
        $client->request(
            'POST',
            '/customer/sms',
            [],
            [],
            [],
            $payload
        );

        $customer = $this->getRepository(Customer::class)->findOneBy(['phone' => $phone]);

        $this->assertEquals($phone, $customer->getPhone()->getValue());
        $this->assertResponseStatusCodeSame(Response::HTTP_NO_CONTENT);
    }

    public function testAccessLoginCheckWithPhone()
    {
        $client = $this->getKernelClient();

        $customerFixture = new CustomerFixtures();
        $this->loadFixture($customerFixture);
        $customer = $customerFixture->getReference('customer_phone');

        $client->request(
            'POST',
            '/customer/sms',
            [],
            [],
            [],
            '{"phone":"79899999999"}'
        );

        $payload = json_encode(['username' => '79899999999', 'password' => $customer->getActiveSms()->getCode()]);

        $client->request(
            'POST',
            '/api/login_check',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $payload
        );

        $this->assertInstanceOf(JWTAuthenticationSuccessResponse::class, $client->getResponse());
    }

    public function testInvalidSmsLoginCheck()
    {
        $client = $this->getKernelClient();

        $customerFixture = new CustomerFixtures();
        $this->loadFixture($customerFixture);
        $customer = $customerFixture->getReference('customer_phone');

        $payload = json_encode(['username' => '79899999999', 'password' => 'wrong']);

        $client->request(
            'POST',
            '/api/login_check',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $payload
        );

        $this->assertInstanceOf(JWTAuthenticationFailureResponse::class, $client->getResponse());
    }

    public function testAccessLoginCheckWithEmail()
    {
        $client = $this->getKernelClient();

        $customerFixture = new CustomerFixtures();
        $this->loadFixture($customerFixture);
        $customer = $customerFixture->getReference('customer_email_password');

        $payload = json_encode(['username' => 'test@mail.mail', 'password' => 'qwerty']);

        $client->request(
            'POST',
            '/api/login_check',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $payload
        );

        $this->assertInstanceOf(JWTAuthenticationSuccessResponse::class, $client->getResponse());
    }

    public function testInvalidPasswordLoginCheck()
    {
        $client = $this->getKernelClient();

        $customerFixture = new CustomerFixtures();
        $this->loadFixture($customerFixture);
        $customer = $customerFixture->getReference('customer_email_password');

        $payload = json_encode(['username' => 'test@mail.mail', 'password' => 'wrong']);

        $client->request(
            'POST',
            '/api/login_check',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $payload
        );

        $this->assertInstanceOf(JWTAuthenticationFailureResponse::class, $client->getResponse());
    }

    private function mockRequest(string $payload): Request
    {
        $request = $this->createMock(Request::class);
        $request->attributes = new ParameterBag();
        $request
            ->method('getContent')
            ->willReturn($payload);

        return $request;
    }

    private function mockParamConverter(): ParamConverter
    {
        $paramConverter = $this->createMock(ParamConverter::class);
        $paramConverter
            ->method('getName')
            ->willReturn('customer');

        return $paramConverter;
    }

    private function mockCustomerRepository(Customer $customer): CustomerRepository
    {
        $customerRepository = $this->createMock(CustomerRepository::class);

        $customerRepository
            ->method('findOneByPhone')
            ->willReturn($customer);

        return $customerRepository;
    }
}
