<?php

namespace App\Tests;

use App\Tests\api\AbstractApiTestCase;
use App\Tests\ApiTester;

class InitCest extends AbstractApiTestCase
{
    public function testInit(ApiTester $tester)
    {
        $tester->sendGet('/init');
        $tester->haveHttpHeader('Content-Type', 'application/json');
        $tester->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $tester->seeResponseIsJson();

        $schema = json_decode($this->getJsonFromFile('init/init'), true);

        $tester->seeResponseMatchesJsonType($schema, '$.data');

        $tester->seeResponseJsonMatchesJsonPath('$.data.categories[4]');
    }
}
