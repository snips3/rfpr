<?php

namespace App\Tests;

use App\Tests\api\AbstractApiTestCase;
use App\Tests\ApiTester;

class ProductCest extends AbstractApiTestCase
{
    public function testProductGet(ApiTester $tester)
    {
        $tester->sendGet('/product/7');
        $tester->haveHttpHeader('Content-Type', 'application/json');
        $tester->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $tester->seeResponseIsJson();

        $schema = json_decode($this->getJsonFromFile('product/product'), true);
        $tester->seeResponseMatchesJsonType(
            $schema,
            '$.data'
        );
    }
}
