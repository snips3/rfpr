<?php

namespace App\Tests\api;

use JsonMachine\JsonMachine;

abstract class AbstractApiTestCase
{
    private string $path;

    public function __construct()
    {
        $this->path = realpath(implode('/', [__DIR__, '../..', 'tests/api/jsonData']));
    }

    protected function getJsonFromFile(string $key)
    {
        return file_get_contents($this->path . '/' . $key . '.json');
    }
}
