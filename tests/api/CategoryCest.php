<?php

namespace App\Tests;

use App\Tests\api\AbstractApiTestCase;
use App\Tests\ApiTester;

class CategoryCest extends AbstractApiTestCase
{
    private int $categoryId;

    public function testCategoryList(ApiTester $tester)
    {
        $tester->sendGet('/category/list');
        $tester->haveHttpHeader('Content-Type', 'application/json');
        $tester->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $tester->seeResponseIsJson();
        $schema = json_decode($this->getJsonFromFile('category/list'), true);
        $tester->seeResponseMatchesJsonType(
            $schema,
            '$.data'
        );

        $this->categoryId = $tester->grabDataFromResponseByJsonPath('$.data[0].id')[0];
    }

    public function testCategoryProducts(ApiTester $tester)
    {
        $tester->sendGet('/category/' . $this->categoryId . '/products');
        $tester->haveHttpHeader('Content-Type', 'application/json');
        $tester->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $tester->seeResponseIsJson();

        $tester->seeResponseMatchesJsonType(['total' => 'integer']);
        $schema = json_decode($this->getJsonFromFile('category/products'), true);
        $tester->seeResponseMatchesJsonType(
            $schema,
            '$.data'
        );
    }
}
