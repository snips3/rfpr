<?php

namespace App\Traits;

trait QueryBuilderTrait
{
    private function clearKeys(array $array): array
    {
        foreach ($array as &$row) {
            $row = $this->clearKeysRow($row);
        }

        return $array;
    }

    private function clearKeysRow(array $row): array
    {
        foreach ($row as $key => $value) {
            $e = explode('_', $key);
            array_pop($e);
            $row[implode('_', $e)] = $value;
            unset($row[$key]);
        }

        return $row;
    }
}
