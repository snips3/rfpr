<?php

namespace App\Utility;

class CircularReferenceHandler
{
    public function __invoke($object)
    {
        return $object->getId();
    }
}
