<?php

namespace App\Utility\Phone;

class Phone
{
    private const PATTERN = '/^((\+?7|8)(?!95[4-79]|99[08]|907|94[^0]|336)([348]\d|9[0-6789]|7[0247])\d{8}|\+?(99[^4568]\d{7,11}|994\d{9}|9955\d{8}|996[57]\d{8}|9989\d{8}|380[34569]\d{8}|375[234]\d{8}|372\d{7,8}|37[0-4]\d{8}))$/';

    private $value = '';

    public function __construct(?string $numberAsString = null)
    {
        if (!empty($numberAsString)) {
            $numberAsString = $this->sanitize($numberAsString);

            if (strlen($numberAsString) === 10) {
                $numberAsString = '7' . $numberAsString;
            }

            if (!$this->isValid($numberAsString)) {
                throw new PhoneException("phone.invalid");
            }

            $this->value = $numberAsString;
        }
    }

    public function isValid(string $numberAsString): bool
    {
        return preg_match(self::PATTERN, $numberAsString);
    }

    private function sanitize(string $numberAsString): int
    {
        return (int) preg_replace('/[^0-9]/', '', $numberAsString);
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
