<?php

namespace App\Utility;

use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class DTO
{
    public function __construct(array $conf = [])
    {
        foreach ($conf as $name => $value) {
            if (is_array($value)) {
                foreach ($value as $kk => $vv) {
                    $name2 = implode('', [$name, ucfirst($kk)]);
                    $this->setValueFromConfig($name2, $vv);
                }
            } else {
                $this->setValueFromConfig($name, $value);
            }
        }
    }

    final private function setValueFromConfig($name, $value): void
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception('Свойство ' . $name . ' не существует в классе ');
        }
    }
}
