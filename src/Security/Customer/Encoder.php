<?php

namespace App\Security\Customer;

use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException;
use Symfony\Component\Security\Core\Encoder\BasePasswordEncoder;
use Symfony\Component\Security\Core\Encoder\EncoderAwareInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class Encoder extends BasePasswordEncoder implements PasswordEncoderInterface
{
    public function encodePassword(string $raw, ?string $salt)
    {
        return password_hash($raw, PASSWORD_ARGON2I);
    }

    public function isPasswordValid(string $customerPassword, string $presentedPassword, ?string $salt)
    {
        //if password
        if (mb_strlen($presentedPassword) !== 4) {
            return !$this->isPasswordTooLong($customerPassword) && password_verify($presentedPassword, $customerPassword);
        }
        //if sms
        return $customerPassword === $presentedPassword;
    }
}
