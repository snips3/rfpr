<?php

namespace App\Security\Customer;

use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\HttpFoundation\JsonResponse;

class Authenticator extends AbstractGuardAuthenticator
{
    public function supports(Request $request)
    {
        return $request->attributes->get('_route') === 'api_login_check'
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
        return $request->getContent();
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $credentials = json_decode($credentials);

        if (null === $credentials || !$credentials->username) {
            // The token header was empty, authentication fails with HTTP Status
            // Code 401 "Unauthorized"
            return null;
        }

        return $userProvider->loadUserByUsername($credentials->username);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function createAuthenticatedToken(UserInterface $user, $providerKey)
    {
        $credentials = json_encode(['username' => $user->getUsername(), 'password' => $user->getPassword()]);

        return new JWTUserToken($user->getRoles(), $user, $credentials, $providerKey);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            // you may want to customize or obfuscate the message first
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData()),

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function supportsRememberMe()
    {
        return false;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
    }
}
