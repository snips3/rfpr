<?php

namespace App\Security\Customer;

use App\Domain\Customer\Repository\CustomerRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class Provider implements UserProviderInterface
{
    private CustomerRepository $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function loadUserByUsername(string $username)
    {
        if (!$customer = $this->customerRepository->findOneBy(['email' => $username])) {
            $customer = $this->customerRepository->findOneBy(['phone' => $username]);
            if (null === $customer) {
                return null;
            }
            //if sms
            return new Identity($username, $customer->getActiveSms()->getCode());
        }
        //if password
        return new Identity($username, $customer->getPassword());
    }

    public function refreshUser(UserInterface $user)
    {
        // TODO: Implement refreshUser() method.
    }

    public function supportsClass(string $class)
    {
        // TODO: Implement supportsClass() method.
    }
}
