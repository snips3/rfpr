<?php

namespace App\Security\Customer;

use Symfony\Component\Security\Core\User\UserInterface;

class Identity implements UserInterface
{
    private string $username;

    private ?string $password;

    public function __construct(string $username, ?string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function eraseCredentials()
    {
    }
}
