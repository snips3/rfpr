<?php

namespace App\DataFixtures;

use App\Domain\Customer\Entity\Customer;
use App\Security\Customer\Encoder;
use App\Utility\Phone\Phone;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CustomerFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $customer = new Customer();
        $phone = new Phone('79899999999');
        $customer->setPhone($phone);

        $manager->persist($customer);

        $this->addReference('customer_phone', $customer);

        $customer = new Customer();
        $email = 'test@mail.mail';
        $password = 'qwerty';

        $passwordEncoder = new Encoder();
        $customer->setEmail($email);
        $customer->setPassword($passwordEncoder->encodePassword($password, null));

        $manager->persist($customer);

        $manager->flush();

        $this->addReference('customer_email_password', $customer);
    }
}
