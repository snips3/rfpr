<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

abstract class ApiException extends \Exception implements HttpExceptionInterface, ApiExceptionInterface
{
    public function getStatusCode()
    {
        return Response::HTTP_INTERNAL_SERVER_ERROR;
    }

    public function getHeaders()
    {
        return [];
    }
}
