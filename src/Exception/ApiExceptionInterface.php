<?php

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

interface ApiExceptionInterface extends \Throwable
{
    public function getDetails(): array;
}
