<?php

namespace App\Command;

use App\Domain\Category\DTO\CategoryDTO;
use App\Domain\Category\Entity\Category;
use App\Domain\Product\DTO\OfferDTO;
use App\Domain\Product\DTO\PriceDTO;
use App\Domain\Product\DTO\ProductDTO;
use App\Domain\Product\Entity\Offer;
use App\Domain\Product\Entity\Price;
use App\Domain\Product\Entity\Product;
use App\Domain\Property\Entity\Property;
use App\Domain\Property\Entity\PropertyName;
use App\Domain\Property\Entity\PropertyValue;
use App\Domain\Shop\DTO\ShopDTO;
use App\Domain\Shop\Entity\Shop;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use JsonMachine\JsonMachine;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Import1cCommand extends Command
{
    const SHOPS = 'shops';
    const CATEGORIES = 'categories';
    const PRODUCTS = 'products';
    const PRODUCT_PROPERTIES = 'products';
    const OFFERS = 'offers';
    const OFFER_PROPERTIES = 'offer_properties';
    const PRICES = 'prices';
    const REMAININGS = 'remainings';
    protected static $defaultName = 'import:1c';
    private EntityManagerInterface $entityManager;
    private LoggerInterface $logger;
    private SerializerInterface $serializer;
    private ValidatorInterface $validator;
    private string $path;
    private SymfonyStyle $io;
    private ProgressBar $memoryBar;

    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $importLogger,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->logger = $importLogger;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->path = realpath(implode('/', [__DIR__, '../..', 'var/data']));

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Parse 1C json files & import data')
            ->addOption(
                'file',
                null,
                InputOption::VALUE_REQUIRED,
                'Choose import file',
            )
            ->setHelp(
                'This command allows Parse 1C json files & import data..., 
             use options to choose type import and action'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        ini_set('memory_limit', '2048M');
        $io = new SymfonyStyle($input, $output);

        /** @var OutputInterface $section */
        $section = $output->section('* Memory Usage');
        $section->writeln('<info>* Memory Usage</info>');
        $this->memoryBar = new ProgressBar($section, 2048);
        $this->memoryBar->setEmptyBarCharacter('░');
        $this->memoryBar->setProgressCharacter('');
        $this->memoryBar->setBarCharacter('▓');

        $fileName = $input->getOption('file');

        $this->logger->notice('Trying to parse ' . $fileName);

        try {
            $this->parsePrices($io, $output);
            //$this->parseProperties($io, $output);
            // $this->parseProducts($io, $output);
        } catch (\Exception $exception) {
            $this->logger->error($exception);
            $io->writeln("");
            $io->error('Error! check logs...', ['exception' => $exception]);

            return Command::FAILURE;
        }

        $io->writeln("");
        $io->success('Import successfully');

        return Command::SUCCESS;
    }

    private function parseProperties(SymfonyStyle $io, OutputInterface $output)
    {
    //    $this->updatePropertyNV($output, 'prodprop', 'product_prop');
    //    $this->updatePropertyNV($output, 'offprop', 'offer_prop');

        $propertyNames = $this->entityManager->getRepository(
            PropertyName::class
        )->findAllAssoc();
        $propertyValues = $this->entityManager->getRepository(
            PropertyValue::class
        )->findAllAssoc();

        /* ~~~~~~~~~~~~~~~~~~~~~~ */
        $section = $output->section('PropertyValue');
        $section->writeln('');
        $section->writeln('<comment>* Product Property</comment>');

        $progressbar = new ProgressBar($section);
        $progressbar->setProgressCharacter('~');

        $ids = [];
        $arr2add = [];

        foreach ($this->getJsonData('prodprop') as $row) {
            $id = $this->entityManager->createQueryBuilder()
                ->select('partial product.{id}')
                ->from(Product::class, 'product')
                ->where('product.uuid = :uuid')
                ->setParameter(':uuid', $row['product_id'])
                ->getQuery()
                ->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);

            if (!$id) {
                continue;
            }
            $ids[] = $id;

            foreach ($row['product_prop'] as $r) {
                if (empty($r['prop_value']) || empty($r['prop_name'])) {
                    continue;
                }

                $progressbar->advance();
                $this->trackMemory();

                $nameId = $this->searchIn($r['prop_name'], $propertyNames);
                $values = [$r['prop_value']];
                if ($nameId === 35) {
                    $values = preg_split('/(;|,)/', trim($r['prop_value']));
                    // $values = explode(';', $r['prop_value']);
                }
                foreach ($values as $value) {
                    $valueId = $this->searchIn($value, $propertyValues);
                    $arr2add[] = implode(',', [$id, $nameId, $valueId]);
                }
            }

            if (count($arr2add) > 100) {
                $this->flushProperties('product', $ids, $arr2add);
            }
        }//end foreach

        $this->flushProperties('product', $ids, $arr2add);
        $progressbar->finish();

        /* ~~~~~~~~~~~~~~~ */
        $progressbar = new ProgressBar($section);
        $progressbar->setProgressCharacter('~');

        $arr2add = [];
        $ids = [];
        foreach ($this->getJsonData('offprop') as $row) {
            if (empty($row['offer_prop'])) {
                continue;
            }
            $offerId = $this->entityManager->createQueryBuilder()
                ->select('partial offer.{id}')
                ->from(Offer::class, 'offer')
                ->where('offer.uuid = :uuid')
                ->setParameter(':uuid', $row['offer_id'])
                ->getQuery()
                ->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);

            if (!$offerId) {
                continue;
            }
            $ids[] = $offerId;
            foreach ($row['offer_prop'] as $r) {
                if (empty($r['prop_value']) || empty($r['prop_name'])) {
                    continue;
                }

                $progressbar->advance();
                $this->trackMemory();

                $nameId = $this->searchIn($r['prop_name'], $propertyNames);
                $values = [$r['prop_value']];
                if ($nameId === 35) {
                    // $values = explode(';', $r['prop_value']);
                    $values = preg_split('/(;|,)/', $r['prop_value']);
                }

                foreach ($values as $value) {
                    $valueId = $this->searchIn($value, $propertyValues);
                    $arr2add[] = implode(',', [$offerId, $nameId, $valueId]);
                }
            }

            if (count($arr2add) > 100) {
                $this->flushProperties('offer', $ids, $arr2add);
            }
        }//end foreach

        $this->flushProperties('offer', $ids, $arr2add);
        $progressbar->finish();
    }

    private function getJsonData(string $key): JsonMachine
    {
        return JsonMachine::fromFile($this->path . '/' . $key . '.json');
    }

    private function searchIn($needle, $arr): ?int
    {
        $needle = trim($needle);
        $needle = mb_strtolower($needle);
        $needle = str_replace('ё', 'е', $needle);

        if (!empty($arr[$needle])) {
            return $arr[$needle];
        }
        return null;
    }

    private function flushProperties(string $key, array &$ids = [], array &$arr2add = [])
    {
        if (empty($ids) || empty($arr2add)) {
            return;
        }
        sort($ids);

        $sql = sprintf(
            'DELETE FROM %s pr WHERE pr.%s IN (%s)',
            Property::class,
            $key,
            implode(',', $ids)
        );

        $this->entityManager->createQuery($sql)->execute();
        $ids = [];

        $sql = "INSERT INTO property (`%s_id`, `property_name_id`, `property_value_id`) VALUES (%s)";
        $sql = sprintf($sql, $key, implode('),(', $arr2add));
        $arr2add = [];
        $this->entityManager->getConnection()->prepare($sql)->execute();
    }

    private function parseShops(SymfonyStyle $io, OutputInterface $output)
    {
        $section = $output->section('Shops');
        $section->writeln('');
        $section->writeln('<comment>* Shops</comment>');

        $progressbar = new ProgressBar($section);
        $progressbar->setProgressCharacter('~');

        foreach ($this->getJsonData('stors') as $row) {
            $shop = Shop::fromUuid(Uuid::fromString($row['id']));
            $shop->setName($row['name']);
            $this->entityManager->persist($shop);

            $progressbar->advance();
        }
        $this->entityManager->flush();
        $progressbar->finish();
    }

    private function parseCategories(SymfonyStyle $io, OutputInterface $output)
    {
        $section = $output->section('Categories');
        $section->writeln('');
        $section->writeln('<comment>* Categories</comment>');

        $categoryRepository = $this->entityManager->getRepository(
            Category::class
        );

        foreach ($this->getJsonData('groups') as $row) {
            $categories[$row['id']] = $row;
        }
        foreach ($categories as $uuid => $row) {
            $category = $categoryRepository->findOneBy(
                ['uuid' => Uuid::fromString($row['id'])]
            );
            if (empty($category)) {
                $category = Category::fromUuid(Uuid::fromString($row['id']));
            }
            $category->setName($row['name']);
            $category->setSlug($row['id']);
            $categories[$uuid]['category'] = $category;
            $this->entityManager->persist($category);
        }
        $this->entityManager->flush();

        foreach ($categories as $uuid => $row) {
            $category = $row['category'];
            if (
                !empty($row['parent_id'])
                && !empty($categories[$row['parent_id']])
            ) {
                $category->setParent(
                    $categories[$row['parent_id']]['category']
                );
            }
            $category->setSlug($category->getId());
        }
        $this->entityManager->flush();
    }

    private function parseProducts(SymfonyStyle $io, OutputInterface $output)
    {
        $section = $output->section('Products');
        $section->writeln('');
        $section->writeln('<comment>* Products</comment>');

        $progressbar = new ProgressBar($section);
        $progressbar->setProgressCharacter('~');

        $categories = [];
        foreach ($this->entityManager->getRepository(Category::class)->findAll() as $category) {
            /** @var Category $category */
            $categories[$category->getUuid()->toString()] = $category->getId();
        }

        foreach ($this->getJsonData('products') as $row) {
            $this->trackMemory();

            if (empty($categories[$row['category_id']])) {
                dump($row['category_id']);
                continue;
            }

            $arr2sql = [
                'uuid' => $row['id'],
                'category_id' => $categories[$row['category_id']],
                'name' => $row['name'],
                'code' => $row['code'],
                'slug' => $row['code'],
                'article' => $row['article'],
                'description' => $row['desc'],
            ];
            $sql = $this->createSql('products', array_keys($arr2sql));
            $stmt = $this->entityManager->getConnection()->prepare($sql);
            foreach ($arr2sql as $key => $value) {
                $stmt->bindValue(':' . $key, $value);
            }
            $stmt->execute();

            if (!empty($row['offers'])) {
                $productId = $this->entityManager->createQueryBuilder()
                    ->select('partial product.{id}')
                    ->from(Product::class, 'product')
                    ->where('product.uuid = :uuid')
                    ->setParameter(':uuid', $row['id'])
                    ->getQuery()
                    ->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);

                $sql = $this->createSql(
                    'offer',
                    [
                        'uuid',
                        'product_id',
                        'bar_code',
                    ]
                );
                $stmt = $this->entityManager->getConnection()->prepare($sql);

                foreach ($row['offers'] as $offer) {
                    $stmt->bindValue(':uuid', $offer['id']);
                    $stmt->bindValue(':product_id', $productId);
                    $stmt->bindValue(':bar_code', $offer['barcode'] ?? null);
                    $stmt->execute();
                }
            }//end if
            $this->trackMemory();
            $progressbar->advance();
        }//end foreach

        $progressbar->finish();
    }

    private function trackMemory(): void
    {
        $this->memoryBar->setProgress(
            ((int) memory_get_peak_usage(true) / 1024 / 1024)
        );
    }

    private function createSql(string $table, array $data): string
    {
        $updateStr = self::arr2upd($data);

        return sprintf(
            "INSERT IGNORE INTO `%s` (%s) VALUES (%s) ON DUPLICATE KEY UPDATE %s",
            $table,
            '`' . implode('`, `', $data) . '`',
            ':' . implode(', :', $data) . '',
            $updateStr
        );
    }

    private function arr2upd(array $arr, ?string $delimiter = ','): string
    {
        $str = [];
        if (is_array($arr)) {
            //$arr = array_filter($arr);
            foreach ($arr as $key) {
                $str[] = "`" . $key . "`=VALUES(`" . $key . "`)";
            }
        }
        $str = implode($delimiter, $str);

        return $str;
    }

    private function parseProductsOld(SymfonyStyle $io, OutputInterface $output)
    {
        $section = $output->section('Products');
        $section->writeln('');
        $section->writeln('<comment>* Products</comment>');

        $progressbar = new ProgressBar($section);
        $progressbar->setProgressCharacter('~');

        $categoryRepository = $this->entityManager->getRepository(
            Category::class
        );
        $productRepository = $this->entityManager->getRepository(
            Product::class
        );
        $offerRepository = $this->entityManager->getRepository(Offer::class);

        foreach ($this->getJsonData('products') as $row) {
            $this->trackMemory();
            $category = $categoryRepository->findOneBy(
                ['uuid' => Uuid::fromString($row['category_id'])]
            );
            $product = $productRepository->findOneBy(
                ['uuid' => Uuid::fromString($row['id'])]
            );

            if (!$category) {
                continue;
            }

            if (!$product) {
                $productDTO = new ProductDTO($row);
                $product = Product::createFromDTO($productDTO);
                $this->entityManager->persist($product);
            }

            $product->setCategory($category);
            foreach ($row['offers'] as $r) {
                $this->trackMemory();
                if (empty($r['barcode'])) {
                    continue;
                }
                $offer = $offerRepository->findOneBy(
                    ['uuid' => Uuid::fromString($r['id'])]
                );

                if (!$offer) {
                    $offerDTO = new OfferDTO($r);
                    $offer = Offer::createFromDTO($offerDTO);
                    $this->entityManager->persist($offer);
                }

                $product->addOffer($offer);
            }
            $this->entityManager->flush();
            $progressbar->advance();
        }//end foreach
        $progressbar->finish();
    }

    private function parseProductsProperties(
        SymfonyStyle $io,
        OutputInterface $output
    ) {
        $section = $output->section('Properties');
        $section->writeln('');
        $section->writeln('<comment>* Properties</comment>');

        $progressbar = new ProgressBar($section);
        $progressbar->setProgressCharacter('~');

        $propertyNameRepository = $this->entityManager->getRepository(
            PropertyName::class
        );
        $propertyValueRepository = $this->entityManager->getRepository(
            PropertyValue::class
        );

        $propertyNameCollection = new ArrayCollection(
            $propertyNameRepository->findAll()
        );
        $propertyValueCollection = new ArrayCollection(
            $propertyValueRepository->findAll()
        );

        $n = 0;
        $arr2add = [];
        foreach ($this->getJsonData('prodprop') as $row) {
            $this->trackMemory();
            /** @var Product $product */
            $productId = $this->entityManager->createQueryBuilder()
                ->select('partial product.{id}')
                ->from(Product::class, 'product')
                ->where('product.uuid = :uuid')
                ->setParameter(':uuid', $row['product_id'])
                ->getQuery()
                ->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);

            if (!$productId) {
                continue;
            }
            if ($n++ < 5700) {
                continue;
            }
            $sql = "DELETE FROM property WHERE product_id = :productId";
            $stmt = $this->entityManager->getConnection()->prepare($sql);
            $stmt->bindValue(':productId', $productId);
            $stmt->execute();

            foreach ($row['product_prop'] as $r) {
                if ($r['prop_name'] === 'Название для я.маркета') {
                    // $product->setNameYandexMarket($r['prop_value']);
                    continue;
                }

                $r['prop_name'] = trim(str_replace('ё', 'е', $r['prop_name']));
                $r['prop_value'] = trim(
                    str_replace('ё', 'е', $r['prop_value'])
                );

                $propertyName = $propertyNameCollection->filter(
                    function (PropertyName $propertyName) use ($r): bool {
                        return !$this->mb_strcasecmp(
                            $propertyName->getValue(),
                            $r['prop_name']
                        );
                    }
                )->first();

                if (!$propertyName) {
                    $propertyName = new PropertyName($r['prop_name']);
                    $propertyNameCollection->add($propertyName);
                    $this->entityManager->persist($propertyName);
                    $this->entityManager->flush();
                }

                $propertyValue = $propertyValueCollection->filter(
                    function (PropertyValue $propertyValue) use ($r): bool {
                        return !$this->mb_strcasecmp(
                            $propertyValue->getValue(),
                            $r['prop_value']
                        );
                    }
                )->first();

                if (!$propertyValue) {
                    $propertyValue = new PropertyValue($r['prop_value']);
                    $propertyValueCollection->add($propertyValue);
                    $this->entityManager->persist($propertyValue);
                    $this->entityManager->flush();
                }

                $arr2add[] = implode(
                    ',',
                    [
                        $productId,
                        $propertyName->getId(),
                        $propertyValue->getId(),
                    ]
                );
            }//end foreach

            if (count($arr2add) > 200) {
                $sql
                    = "INSERT INTO property (`product_id`, `property_name_id`, `property_value_id`) VALUES (%s)";
                $sql = sprintf($sql, implode('),(', $arr2add));
                $arr2add = [];
                $this->entityManager->getConnection()->prepare($sql)->execute();
            }

            $progressbar->advance();
        }//end foreach

        if (!empty($arr2add)) {
            $sql
                = "INSERT INTO property (`product_id`, `property_name_id`, `property_value_id`) VALUES (%s)";
            $sql = sprintf($sql, implode('),(', $arr2add));
            $arr2add = [];
            $this->entityManager->getConnection()->prepare($sql)->execute();
        }
        $progressbar->finish();
    }

    private function mb_strcasecmp($str1, $str2, $encoding = null): int
    {
        if (null === $encoding) {
            $encoding = mb_internal_encoding();
        }

        $str1 = trim(str_replace('ё', 'е', $str1));
        $str2 = trim(str_replace('ё', 'е', $str2));

        return strcmp(
            mb_strtoupper($str1, $encoding),
            mb_strtoupper($str2, $encoding)
        );
    }

    private function parseProductsPropertiesColors(
        SymfonyStyle $io,
        OutputInterface $output
    ) {
        $section = $output->section('Properties');
        $section->writeln('');
        $section->writeln('<comment>* Properties</comment>');

        $progressbar = new ProgressBar($section);
        $progressbar->setProgressCharacter('~');

        $propertyNameRepository = $this->entityManager->getRepository(
            PropertyName::class
        );
        $propertyValueRepository = $this->entityManager->getRepository(
            PropertyValue::class
        );

        $propertyNameCollection = new ArrayCollection(
            $propertyNameRepository->findAll()
        );
        $propertyValueCollection = new ArrayCollection(
            $propertyValueRepository->findAll()
        );

        $n = 0;
        $arr2add = [];
        foreach ($this->getJsonData('prodprop') as $row) {
            $progressbar->advance();
            $this->trackMemory();
            /** @var Product $product */
            $productId = $this->entityManager->createQueryBuilder()
                ->select('partial product.{id}')
                ->from(Product::class, 'product')
                ->where('product.uuid = :uuid')
                ->setParameter(':uuid', $row['product_id'])
                ->getQuery()
                ->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);

            if (!$productId) {
                continue;
            }
            /*
            $sql = "DELETE FROM property WHERE product_id = :productId";
            $stmt = $this->entityManager->getConnection()->prepare($sql);
            $stmt->bindValue(':productId', $productId);
            $stmt->execute();*/

            foreach ($row['product_prop'] as $r) {
                if ($r['prop_name'] === 'Название для я.маркета') {
                    // $product->setNameYandexMarket($r['prop_value']);
                    continue;
                }
                if ($r['prop_name'] !== 'Цвет для фильтра') {
                    continue;
                }

                $sql
                    = "DELETE FROM property WHERE property_name_id=35 AND product_id = :productId";
                $stmt = $this->entityManager->getConnection()->prepare($sql);
                $stmt->bindValue(':productId', $productId);

                $r['prop_name'] = trim(str_replace('ё', 'е', $r['prop_name']));
                $r['prop_value'] = trim(
                    str_replace('ё', 'е', $r['prop_value'])
                );

                $propertyName = $propertyNameCollection->filter(
                    function (PropertyName $propertyName) use ($r): bool {
                        return !$this->mb_strcasecmp(
                            $propertyName->getValue(),
                            $r['prop_name']
                        );
                    }
                )->first();

                $values = explode(';', $r['prop_value']);
                $values = array_map('trim', $values);
                foreach ($values as $value) {
                    $propertyValue = $propertyValueCollection->filter(
                        function (PropertyValue $propertyValue) use ($value
                        ): bool {
                            return !$this->mb_strcasecmp(
                                $propertyValue->getValue(),
                                $value
                            );
                        }
                    )->first();

                    if (!$propertyValue) {
                        $propertyValue = new PropertyValue($value);
                        $propertyValueCollection->add($propertyValue);
                        $this->entityManager->persist($propertyValue);
                        $this->entityManager->flush();
                    }

                    $arr2add[] = implode(
                        ',',
                        [
                            $productId,
                            $propertyName->getId(),
                            $propertyValue->getId(),
                        ]
                    );
                }//end foreach
            }//end foreach

            if (count($arr2add) > 200) {
                $sql
                    = "INSERT INTO property (`product_id`, `property_name_id`, `property_value_id`) VALUES (%s)";
                $sql = sprintf($sql, implode('),(', $arr2add));
                $arr2add = [];
                $this->entityManager->getConnection()->prepare($sql)->execute();
            }
        }//end foreach

        if (!empty($arr2add)) {
            $sql
                = "INSERT INTO property (`product_id`, `property_name_id`, `property_value_id`) VALUES (%s)";
            $sql = sprintf($sql, implode('),(', $arr2add));
            $arr2add = [];
            $this->entityManager->getConnection()->prepare($sql)->execute();
        }
        $progressbar->finish();
    }

    private function parseOffersProperties(
        SymfonyStyle $io,
        OutputInterface $output
    ) {
        $section = $output->section('Properties');
        $section->writeln('');
        $section->writeln('<comment>* Offers Properties</comment>');

        $progressbar = new ProgressBar($section);
        $progressbar->setProgressCharacter('~');

        $propertyNameRepository = $this->entityManager->getRepository(
            PropertyName::class
        );
        $propertyValueRepository = $this->entityManager->getRepository(
            PropertyValue::class
        );

        $propertyNameCollection = new ArrayCollection(
            $propertyNameRepository->findAll()
        );
        $propertyValueCollection = new ArrayCollection(
            $propertyValueRepository->findAll()
        );

        $n = 0;
        $arr2add = [];
        foreach ($this->getJsonData('offprop') as $row) {
            if ($n++ < 12000) {
                continue;
            }

            $this->trackMemory();
            /** @var Product $product */
            $offerId = $this->entityManager->createQueryBuilder()
                ->select('partial offer.{id}')
                ->from(Offer::class, 'offer')
                ->where('offer.uuid = :uuid')
                ->setParameter(':uuid', $row['offer_id'])
                ->getQuery()
                ->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);

            if (!$offerId) {
                continue;
            }
            $offerIds[] = $offerId;

            foreach ($row['offer_prop'] as $r) {
                $r['prop_name'] = trim(str_replace('ё', 'е', $r['prop_name']));
                $r['prop_value'] = trim(
                    str_replace('ё', 'е', $r['prop_value'])
                );

                $propertyName = $propertyNameCollection->filter(
                    function (PropertyName $propertyName) use ($r): bool {
                        return !$this->mb_strcasecmp(
                            $propertyName->getValue(),
                            $r['prop_name']
                        );
                    }
                )->first();

                if (!$propertyName) {
                    $propertyName = new PropertyName($r['prop_name']);
                    $propertyNameCollection->add($propertyName);
                    $this->entityManager->persist($propertyName);
                    $this->entityManager->flush();
                }

                $propertyValue = $propertyValueCollection->filter(
                    function (PropertyValue $propertyValue) use ($r): bool {
                        return !$this->mb_strcasecmp(
                            $propertyValue->getValue(),
                            $r['prop_value']
                        );
                    }
                )->first();

                if (!$propertyValue) {
                    $propertyValue = new PropertyValue($r['prop_value']);
                    $propertyValueCollection->add($propertyValue);
                    $this->entityManager->persist($propertyValue);
                    $this->entityManager->flush();
                }

                $arr2add[] = implode(
                    ',',
                    [
                        $offerId,
                        $propertyName->getId(),
                        $propertyValue->getId(),
                    ]
                );
            }//end foreach

            if (count($arr2add) > 200) {
                if (!empty($offerIds)) {
                    $sql = "DELETE FROM property WHERE offer_id IN (:offerIds)";
                    $stmt = $this->entityManager->getConnection()->prepare(
                        $sql
                    );
                    $stmt->bindValue(':offerIds', implode(',', $offerIds));
                    $stmt->execute();
                    $offerIds = [];
                }

                $sql
                    = "INSERT INTO property (`offer_id`, `property_name_id`, `property_value_id`) VALUES (%s)";
                $sql = sprintf($sql, implode('),(', $arr2add));
                $arr2add = [];
                $this->entityManager->getConnection()->prepare($sql)->execute();
            }

            $progressbar->advance();
        }//end foreach

        if (!empty($arr2add)) {
            if (!empty($offerIds)) {
                $sql = "DELETE FROM property WHERE offer_id IN (:offerIds)";
                $stmt = $this->entityManager->getConnection()->prepare($sql);
                $stmt->bindValue(':offerIds', implode(',', $offerIds));
                $stmt->execute();
                $offerIds = [];
            }

            $sql
                = "INSERT INTO property (`offer_id`, `property_name_id`, `property_value_id`) VALUES (%s)";
            $sql = sprintf($sql, implode('),(', $arr2add));
            $arr2add = [];
            $this->entityManager->getConnection()->prepare($sql)->execute();
        }
        $progressbar->finish();
    }

    private function parseOffersPropertiesOld(
        SymfonyStyle $io,
        OutputInterface $output
    ) {
        $section = $output->section('Properties');
        $section->writeln('');
        $section->writeln('<comment>* Offers Properties</comment>');

        $progressbar = new ProgressBar($section, 25841);
        $progressbar->setProgressCharacter('~');

        $offerRepository = $this->entityManager->getRepository(Offer::class);
        $propertyNameRepository = $this->entityManager->getRepository(
            PropertyName::class
        );
        $propertyValueRepository = $this->entityManager->getRepository(
            PropertyValue::class
        );

        $propertyNameCollection = new ArrayCollection(
            $propertyNameRepository->findAll()
        );
        $propertyValueCollection = new ArrayCollection(
            $propertyValueRepository->findAll()
        );

        $num = 0;
        foreach ($this->getJsonData('offprop') as $row) {
            $this->trackMemory();
            if (++$num < 0) {
                $progressbar->advance();
                continue;
            }
            /** @var Offer $offer */
            $offer = $offerRepository->findOneBy(
                ['uuid' => Uuid::fromString($row['offer_id'])]
            );
            if (!$offer) {
                continue;
            }
            $offer->getProperties()->clear();

            foreach ($row['offer_prop'] as $r) {
                $r['prop_name'] = trim(str_replace('ё', 'е', $r['prop_name']));
                $r['prop_value'] = trim(
                    str_replace('ё', 'е', $r['prop_value'])
                );

                $propertyName = $propertyNameCollection->filter(
                    function (PropertyName $propertyName) use ($r): bool {
                        $value = mb_strtolower($propertyName->getValue());
                        $value = str_replace('ё', 'е', $value);

                        return $value === mb_strtolower($r['prop_name']);
                    }
                )->first();

                if (!$propertyName) {
                    $propertyName = new PropertyName($r['prop_name']);
                    $propertyNameCollection->add($propertyName);
                }

                $propertyValue = $propertyValueCollection->filter(
                    function (PropertyValue $propertyValue) use ($r): bool {
                        $value = mb_strtolower($propertyValue->getValue());
                        $value = str_replace('ё', 'е', $value);

                        return $value === mb_strtolower($r['prop_value']);
                    }
                )->first();

                if (!$propertyValue) {
                    $propertyValue = new PropertyValue($r['prop_value']);
                    $propertyValueCollection->add($propertyValue);
                }

                $property = new Property($propertyName, $propertyValue);
                $offer->addProperty($property);
            }//end foreach

            if (($num % 100) === 0) {
                $this->entityManager->flush();
            }

            $progressbar->advance();
        }//end foreach
        $this->entityManager->flush();
        $progressbar->finish();
    }

    private function parseStocks(SymfonyStyle $io, OutputInterface $output)
    {
        $section = $output->section('Stocks');
        $section->writeln('');
        $section->writeln('<comment>* Stocks</comment>');

        $progressbar = new ProgressBar($section);
        $progressbar->setProgressCharacter('~');

        $shops = $this->entityManager->getRepository(Shop::class)->findAll();
        $offerRepository = $this->entityManager->getRepository(Offer::class);

        foreach ($this->getJsonData('stocks') as $row) {
            $this->trackMemory();

            $sql = "INSERT IGNORE INTO `price` (`offer_id`, `shop_id`, `quantity`) VALUES (
            (SELECT `id` FROM `offer` WHERE uuid = '%s'),
            (SELECT `id` FROM `shop` WHERE uuid = '%s'),
            %d ) ON DUPLICATE KEY UPDATE quantity=%d, updated_at = CURRENT_TIMESTAMP ";
            $sql = sprintf(
                $sql,
                $row['offer_id'],
                $row['store_id'],
                (int) $row['stock'],
                (int) $row['stock']
            );

            $this->entityManager->getConnection()->executeQuery($sql);
            $progressbar->advance();
        }
        $this->entityManager->flush();
        $progressbar->finish();
    }

    private function parsePrices(SymfonyStyle $io, OutputInterface $output)
    {
        $section = $output->section('Prices');
        $section->writeln('');
        $section->writeln('<comment>* Prices</comment>');

        $progressbar = new ProgressBar($section);
        $progressbar->setProgressCharacter('~');

        $arr2sql = [];
        $sql
            = "INSERT IGNORE INTO `price` (`offer_id`, `shop_id`, `actual`, `old`, `quantity`) VALUES ( %s ) ON DUPLICATE KEY UPDATE actual=VALUES(actual), old=VALUES(old), updated_at = CURRENT_TIMESTAMP;";
        foreach ($this->getJsonData('prices') as $row) {
            $values
                = "(SELECT `id` FROM `offer` WHERE uuid = '%s'), (SELECT `id` FROM `shop` WHERE uuid = '%s'), %d, %d, 1";
            $arr2sql[] = sprintf(
                $values,
                $row['offer_id'],
                $row['store_id'],
                (int) $row['price'],
                (int) $row['priceOld'],
                (int) $row['price'],
                (int) $row['priceOld']
            );

            $progressbar->advance();

            if (count($arr2sql) > 500) {
                $string = sprintf($sql, implode('),(', $arr2sql));
                $this->trackMemory();
                file_put_contents(
                    "/var/www/var/prices.sql",
                    $string,
                    FILE_APPEND
                );
                $arr2sql = [];
            }
        }//end foreach

        $string = sprintf($sql, implode('),(', $arr2sql));
        $this->trackMemory();
        file_put_contents("/var/www/var/prices.sql", $string, FILE_APPEND);

        $progressbar->finish();
    }

    private function createCategories(SymfonyStyle $io)
    {
        $progressBar = $io->createProgressBar();
        $categoryRepository = $this->entityManager->getRepository(
            Category::class
        );
        foreach ($this->getCategoriesData(self::CATEGORIES) as $key => $row) {
            $row['slug'] = ($row['slug'] ?? $row['name']);
            $categoryDTO = new CategoryDTO();
            $this->serializer->deserialize(
                json_encode($row),
                CategoryDTO::class,
                'json',
                [AbstractNormalizer::OBJECT_TO_POPULATE => $categoryDTO],
            );
            //TODO custom exception?
            $errors = $this->validator->validate($categoryDTO);
            if ($errors->count()) {
                throw new \Exception($errors);
            }
            $category = $this->entityManager->getRepository(Category::class)
                ->findOneBy(['uuid' => $row['uuid']]);
            if (null === $category) {
                $category = Category::createFromDTO($categoryDTO);
            } else {
                $category = $category->updateFromDTO($categoryDTO);
            }

            $this->entityManager->persist($category);
            $progressBar->advance();
        }//end foreach
        $this->entityManager->flush();
        $progressBar->finish();

        $this->logger->info(
            'Categories import successfully! Count changed: ' . $key
        );

        $progressBar = $io->createProgressBar();
        foreach ($this->getCategoriesData(self::CATEGORIES) as $key => $row) {
            if (empty($row['parent_uuid'])) {
                continue;
            }
            $category = $categoryRepository->findOneBy(
                ['uuid' => Uuid::fromString($row['uuid'])]
            );
            $parent = $categoryRepository->findOneBy(
                ['uuid' => Uuid::fromString($row['parent_uuid'])]
            );
            $category->setParent($parent);

            $progressBar->advance();
        }
        $this->entityManager->flush();
        $progressBar->finish();

        $this->logger->info('Categories parent import successfully!');
    }

    private function createShops(SymfonyStyle $io)
    {
        $progressBar = $io->createProgressBar();
        foreach ($this->getCategoriesData(self::SHOPS) as $key => $row) {
            $shopDTO = new ShopDTO();
            $this->serializer->deserialize(
                json_encode($row),
                ShopDTO::class,
                'json',
                [AbstractNormalizer::OBJECT_TO_POPULATE => $shopDTO]
            );
            $errors = $this->validator->validate($shopDTO);
            if ($errors->count()) {
                throw new \Exception($errors);
            }

            $shop = $this->entityManager->getRepository(Shop::class)->findOneBy(
                ['uuid' => $row['uuid']]
            );
            if (null === $shop) {
                $shop = Shop::createFromDTO($shopDTO);
            } else {
                $shop->updateFromDTO($shopDTO);
            }

            $this->entityManager->persist($shop);

            $progressBar->advance();
        }//end foreach
        $this->entityManager->flush();
        $progressBar->finish();

        $this->logger->info('Shops import successfully! Count changed: ' . $key);
    }

    private function createProducts(SymfonyStyle $io)
    {
        $progressBar = $io->createProgressBar();

        foreach ($this->getCategoriesData(self::PRODUCTS) as $key => $row) {
            $row['slug'] = ($row['slug'] ?? $row['name']);
            $productDTO = new ProductDTO();
            $this->serializer->deserialize(
                json_encode($row),
                ProductDTO::class,
                'json',
                [AbstractNormalizer::OBJECT_TO_POPULATE => $productDTO],
            );
            $errors = $this->validator->validate($productDTO);
            if ($errors->count()) {
                throw new \Exception($errors);
            }

            $product = ($this->entityManager->getRepository(Product::class)
                ->findOneBy(['uuid' => $row['uuid']]));

            if (null === $product) {
                $product = Product::createFromDTO($productDTO);
            } else {
                $product->updateFromDTO($productDTO);
            }

            $this->entityManager->persist($product);

            if (($key % 100) === 0) {
                $this->entityManager->flush();
                // Detaches all objects from Doctrine!
                $this->entityManager->clear();
            }

            $progressBar->advance();
        }//end foreach
        $this->entityManager->flush();
        $progressBar->finish();

        $this->logger->info(
            'Products import successfully! Count changed: ' . $key
        );
    }

    private function createOffers(SymfonyStyle $io)
    {
        $progressBar = $io->createProgressBar();
        $productRepository = $this->entityManager->getRepository(
            Product::class
        );
        $offerRepository = $this->entityManager->getRepository(Offer::class);

        foreach ($this->getCategoriesData(self::OFFERS) as $key => $row) {
            $offerDTO = new OfferDTO();
            $this->serializer->deserialize(
                json_encode($row),
                ProductDTO::class,
                'json',
                [AbstractNormalizer::OBJECT_TO_POPULATE => $offerDTO],
            );
            $errors = $this->validator->validate($offerDTO);
            if ($errors->count()) {
                throw new \Exception($errors);
            }
            $offer = $offerRepository->findOneBy(['uuid' => $row['uuid']]);
            $product = $productRepository->findOneBy(
                ['uuid' => Uuid::fromString($offerDTO->product_uuid)]
            );

            if (null === $offer) {
                $offer = Offer::createFromDTO($offerDTO);
            } else {
                $offer = $offer->updateFromDTO($offerDTO);
            }
            $offer->setProduct($product);

            $this->entityManager->persist($offer);
        }//end foreach
        $this->entityManager->flush();
        $progressBar->finish();

        $this->logger->info('Offers import successfully! Count changed: ' . $key);
    }

    private function createProductProperties(SymfonyStyle $io)
    {
        $progressBar = $io->createProgressBar();
        $productRepository = $this->entityManager->getRepository(
            Product::class
        );
        $propertyNameRepository = $this->entityManager->getRepository(
            PropertyName::class
        );
        $propertyValueRepository = $this->entityManager->getRepository(
            PropertyValue::class
        );

        foreach (
            $this->getCategoriesData(self::PRODUCT_PROPERTIES) as $key => $row
        ) {
            $product = $productRepository->findOneBy(
                ['uuid' => $row['product_uuid']]
            );

            $propertyName = $propertyNameRepository->findOneBy(
                ['value' => $row['property_name']]
            ) ?? new PropertyName($row['property_name']);
            $propertyValue = $propertyValueRepository->findOneBy(
                ['value' => $row['property_value']]
            ) ?? new PropertyValue($row['property_value']);

            $property = new Property($propertyName, $propertyValue);

            $product->addProperty($property);

            $this->entityManager->persist($property);
        }

        $this->entityManager->flush();
        $progressBar->finish();

        $this->logger->info(
            'Product Properties import successfully! Count changed: ' . $key
        );
    }

    private function createOfferProperties(SymfonyStyle $io)
    {
        $progressBar = $io->createProgressBar();
        foreach (
            $this->getCategoriesData(self::OFFER_PROPERTIES) as $key => $row
        ) {
            $offer = $this->entityManager->getRepository(Offer::class)
                ->findOneBy(['uuid' => $row['offer_uuid']]);
            $property = new Property();
            $propertyName = new PropertyName();
            $propertyValue = new PropertyValue();

            $propertyName->setValue($row['property_name']);
            $propertyValue->setValue($row['property_value']);

            $property->setName($propertyName);
            $property->setValue($propertyValue);
            $property->setProduct($offer);
            $offer->addProperty($property);

            $this->entityManager->persist($offer);
        }

        $this->entityManager->flush();
        $progressBar->finish();

        $this->logger->info(
            'Offer Properties import successfully! Count changed: ' . $key
        );
    }

    private function createPrices(SymfonyStyle $io)
    {
        foreach ($this->getCategoriesData(self::PRICES) as $key => $row) {
            $priceDTO = new PriceDTO();
            $this->serializer->deserialize(
                json_encode($row),
                PriceDTO::class,
                'json',
                [AbstractNormalizer::OBJECT_TO_POPULATE => $priceDTO],
            );
            $errors = $this->validator->validate($priceDTO);
            if ($errors->count()) {
                throw new \Exception($errors);
            }

            $offer = $this->entityManager->getRepository(Offer::class)
                ->findOneBy(['uuid' => $row['offer_uuid']]);
            $shop = $this->entityManager->getRepository(Shop::class)->findOneBy(
                ['uuid' => $row['shop_uuid']]
            );

            $price = $this->entityManager->createQueryBuilder()
                ->select('u')
                ->from('App\Domain\Product\Entity\Price', 'u')
                ->andWhere('u.offer = ?1')
                ->andWhere('u.shop = ?2')
                ->setParameter(1, $offer)
                ->setParameter(2, $shop)
                ->getQuery()
                ->getSingleResult();

            if (null === $price) {
                $price = Price::createFromDTO($priceDTO);
            } else {
                $price = $price->updateFromDTO($priceDTO);
            }
            $price->setOffer($offer);
            $price->setShop($shop);

            $this->entityManager->persist($price);
        }//end foreach
        $this->logger->info(
            'Remainings import successfully! Count changed: ' . $key
        );
        foreach ($this->getCategoriesData(self::REMAININGS) as $key => $row) {
            $offer = $this->entityManager->getRepository(Offer::class)
                ->findOneBy(['uuid' => $row['offer_uuid']]);
            $shop = $this->entityManager->getRepository(Shop::class)->findOneBy(
                ['uuid' => $row['shop_uuid']]
            );

            $price = $this->entityManager->createQueryBuilder()
                ->select('u')
                ->from('App\Domain\Product\Entity\Price', 'u')
                ->andWhere('u.offer = ?1')
                ->andWhere('u.shop = ?2')
                ->setParameter(1, $offer)
                ->setParameter(2, $shop)
                ->getQuery()
                ->getSingleResult();

            $price->setQuantity($row['quantity']);

            $this->entityManager->persist($price);
        }//end foreach
        $progressBar = $io->createProgressBar();

        $this->entityManager->flush();
        $progressBar->finish();

        $this->logger->info(
            'Remainings import successfully! Count changed: ' . $key
        );
    }

    private function arr2add(array $arr): string
    {
        foreach ($arr as $k => $v) {
            $arr[$k] = implode('', [':', $k]);
        }

        return "(`" . implode("`,`", array_keys($arr)) . "`) VALUES(" . implode(
            ',',
            $arr
        ) . ")";
    }

    private function getPropertyNameByValue(
        ArrayCollection &$propertyNameCollection,
        string $value
    ): ?PropertyName {
        $value = trim(str_replace('ё', 'е', $value));

        return $propertyNameCollection->filter(
            function (PropertyName $propertyName) use ($value): bool {
                return !$this->mb_strcasecmp($propertyName->getValue(), $value);
            }
        )->first();
    }

    private function getPropertyValueByValue(
        ArrayCollection &$propertyValueCollection,
        string $value
    ): ?PropertyValue {
        $value = trim(str_replace('ё', 'е', $value));

        return $propertyValueCollection->filter(
            function (PropertyValue $propertyValue) use ($value): bool {
                return !$this->mb_strcasecmp(
                    $propertyValue->getValue(),
                    $value
                );
            }
        )->first();
    }

    private function updatePropertyNV(
        OutputInterface $output,
        string $file = 'prodprop',
        string $rowKey = 'product_prop'
    ) {
        $arr2fix = ['желый' => 'Жёлтый'];
        $section = $output->section($file);
        $section->writeln('');
        $section->writeln("<comment>* [$file]</comment>");

        $progressbar = new ProgressBar($section);
        $progressbar->setProgressCharacter('~');

        $arr2add = [];
        $arr2explode = [35];
        $it = [
            'prop_name' => [
                'table' => 'property_name',
                'data' => $this->entityManager->getRepository(
                    PropertyName::class
                )->findAllAssoc(),
            ],
            'prop_value' => [
                'table' => 'property_value',
                'data' => $this->entityManager->getRepository(
                    PropertyValue::class
                )->findAllAssoc(),
            ],
        ];

        // dd(array_slice($it['prop_value']['data'], 18000, 2000));
        foreach ($this->getJsonData($file) as $row) {
            if (empty($row[$rowKey])) {
                continue;
            }
            foreach ($row[$rowKey] as $r) {
                $progressbar->advance();
                $this->trackMemory();

                $propertyName = $r['prop_name'] ?? null;

                if ($propertyName === 'Название для я.маркета"') {
                    continue;
                }

                $propertyNameId = $this->searchIn(
                    $propertyName,
                    $it['prop_name']['data']
                );

                foreach ($it as $key => &$val) {
                    $this->trackMemory();
                    if (empty($r[$key])) {
                        continue;
                    }

                    if (
                        $key === 'prop_value' && !empty($propertyNameId)
                        && in_array($propertyNameId, $arr2explode)
                    ) {
                        // $originals = explode(';', trim($r[$key]));
                        $originals = preg_split('/(;|,)/', trim($r[$key]));
                    } else {
                        $originals = [$r[$key]];
                    }
                    foreach ($originals as $original) {
                        $original = trim($original);

                        $value = $original;
                        $value = mb_strtolower($value);
                        $value = str_replace('ё', 'е', $value);

                        if (!array_key_exists($value, $val['data'])) {
                            $val['data'][$value] = -1;
                            $arr2add[$val['table']][] = sprintf(
                                '\'%s\'',
                                addslashes($original)
                            );
                        }
                    }
                }//end foreach
            }//end foreach

            foreach ($arr2add as $table => $data) {
                if (count($data) >= 200) {
                    $sql = "INSERT INTO %s (`value`) VALUES (%s)";
                    $sql = sprintf($sql, $table, implode('),(', $data));
                    $this->entityManager->getConnection()->prepare($sql)
                        ->execute();
                    unset($arr2add[$table]);
                }
            }
        }//end foreach

        foreach ($arr2add as $table => $data) {
            if (empty($data)) {
                continue;
            }
            $sql = "INSERT INTO %s (`value`) VALUES (%s)";
            $sql = sprintf($sql, $table, implode('),(', $data));
            $this->entityManager->getConnection()->prepare($sql)->execute();
        }

        $this->trackMemory();
    }
}
