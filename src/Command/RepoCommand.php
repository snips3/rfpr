<?php

namespace App\Command;

use App\Domain\Category\Entity\Category;
use App\Domain\City\Entity\City;
use App\Domain\Product\Entity\Product;
use App\Domain\Property\Entity\Property;
use App\Domain\Property\Entity\PropertyName;
use App\Domain\Property\Entity\PropertyValue;
use App\Domain\Shop\Entity\Shop;
use App\Domain\Category\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use JsonMachine\JsonMachine;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RepoCommand extends Command
{
    protected static $defaultName = 'r:r';

    private EntityManagerInterface $entityManager;
    private CategoryRepository $categoryRepository;
    private string $path;

    public function __construct(EntityManagerInterface $entityManager, CategoryRepository $categoryRepository)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->categoryRepository = $categoryRepository;
        $this->path = realpath(implode('/', [__DIR__, '../..', 'var/data']));
    }

    protected function configure()
    {
        $this
            ->setDescription('Parse 1C json files & import data')
            // ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            // ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        ini_set('memory_limit', '2048M');
        $io = new SymfonyStyle($input, $output);

        //$category = $this->categoryRepository->find(417);
        //$this->categoryRepository->getFiltersForCategory($category);

        $this->updateProductsProperties($io);

        $io->writeln("");
        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }

    private function getCategoriesData(string $key): JsonMachine
    {
        return JsonMachine::fromFile($this->path. '/'.$key.'.json');
    }

    private function createCategories(SymfonyStyle $io)
    {
        $progressBar = $io->createProgressBar();
        foreach ($this->getCategoriesData('groups') as $key => $row) {
            $category = new Category(Uuid::fromString($row['uuid']));
            $category
                ->setName($row['name'])
                ->setSlug($row['name'])
                ->setActive($row['enabled'] === 'true')
            ;
            $this->entityManager->persist($category);
            $progressBar->advance();
        }
        $this->entityManager->flush();
        $progressBar->finish();
    }

    private function updateCategories(SymfonyStyle $io)
    {
        $progressBar = $io->createProgressBar();
        foreach ($this->getCategoriesData('groups') as $key => $row) {
            if (empty($row['parent_uuid'])) {
                continue;
            }
            $category = $this->categoryRepository->findOneBy(['uuid' => Uuid::fromString($row['uuid'])]);
            $parent =  $this->categoryRepository->findOneBy(['uuid' => Uuid::fromString($row['parent_uuid'])]);
            $category->setParent($parent);

            $progressBar->advance();
        }
        $this->entityManager->flush();
        $progressBar->finish();
    }

    private function createShops(SymfonyStyle $io)
    {
        $city = new City();
        $city->setName('Новосибирск');
        $city->setDomain('nsk');
        $this->entityManager->persist($city);

        $progressBar = $io->createProgressBar();
        foreach ($this->getCategoriesData('stors') as $key => $row) {
            $shop = new Shop();
            $shop
                ->setUuid(Uuid::fromString($row['uuid']))
                ->setName($row['name'])
                ->setCity(!empty($row['city']) && $row['city'] === 'Новосибирск' ? $city : null)
                ->setAddress($row['address'] ?? null)
                ->setGeo($row['geo'] ?? null)
            ;
            $this->entityManager->persist($shop);

            $progressBar->advance();
        }
        $this->entityManager->flush();
        $progressBar->finish();
    }

    private function createProducts(SymfonyStyle $io)
    {
        $categories = new ArrayCollection($this->categoryRepository->findAll());
        $progressBar = $io->createProgressBar();

        foreach ($this->getCategoriesData('products') as $key => $row) {
            unset($row['properties']);
            // dump($row);die;
            $category = $categories->filter(function (Category $category) use ($row): bool {
                return $category->getUuid()->toString() === $row['groups'][0];
            })->first();
            if (!$category instanceof Category) {
                //$io->writeln("");
                //$io->warning(sprintf("Category not found: %s for product %s", $row['groups'][0], $row['uuid']));

                continue;
            }
            $product = new Product();
            $product
                ->setUuid(Uuid::fromString($row['uuid']))
                ->setName($row['name'])
                ->setSlug($row['name'])
                ->setDescription($row['description'])
                ->setArticle($row['article'])
                ->setCode($row['product_code'])
                ->setCategory($category)
                ->setActive($row['enabled'] === 'true')
                ->setImage(str_replace("&bpsp;", "", $row['image']))
            ;
            $this->entityManager->persist($product);

            if (($key % 100) === 0) {
                $this->entityManager->flush();
                $this->entityManager->clear(); // Detaches all objects from Doctrine!
                $categories = new ArrayCollection($this->categoryRepository->findAll());
            }

            $progressBar->advance();
        }
        $this->entityManager->flush();
        $progressBar->finish();
    }

    private function updateProductsProperties(SymfonyStyle $io)
    {
        $progressBar = $io->createProgressBar();
        $progressBar->setMaxSteps(49554);

        $propertyNames = new ArrayCollection($this->entityManager->getRepository(PropertyName::class)->findAll());
        $propertyValues = new ArrayCollection($this->entityManager->getRepository(PropertyValue::class)->findAll());

        foreach ($this->getCategoriesData('products') as $key => $row) {
            /** @var Product $product */
            $product = $this->entityManager->getRepository(Product::class)->findOneBy(['uuid' => $row['uuid']]);
            if (!$product) {
                dump($row);
                continue;
            }
            $product->getProperties()->clear();

            foreach ($row['properties'] as $r) {
                if (!trim($r['value']) || $r['value'] === '-') {
                    continue;
                }

                $propertyName = $propertyNames->filter(
                    function (PropertyName $propertyName) use ($r): bool {
                        //dump($propertyName->getValue(), $r['name'], $propertyName->getValue() === $r['name']);
                        return strcasecmp($propertyName->getValue(), $r['name']);
                    }
                )->first();
                if (!$propertyName) {
                    dump($r['name']);
                    $propertyName = new PropertyName($r['name']);
                    $propertyNames->add($propertyName);
                    $this->entityManager->persist($propertyName);
                }

                $propertyValue = $propertyValues->filter(
                    function (PropertyValue $propertyValue) use ($r): bool {
                        return strcasecmp($propertyValue->getValue(), $r['value']);
                    }
                )->first();
                if (!$propertyValue) {
                    $propertyValue = new PropertyValue($r['value']);
                    $propertyValues->add($propertyValue);
                    $this->entityManager->persist($propertyValue);
                }

                $product->addProperty(new Property($propertyName, $propertyValue));
            }

            if (($key % 100) === 0) {
                $this->entityManager->flush();
                // $this->entityManager->clear(); // Detaches all objects from Doctrine!
            }

            $progressBar->advance();
        }
        $this->entityManager->flush();
        $progressBar->finish();
    }
}
