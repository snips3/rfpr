<?php

namespace App\Command;

use DomDocument;
use DOMImplementation;
use DOMXPath;
use Exception;

class Array2XML
{
    public $xmlVersion = "1.0";
    public $encoding = 'UTF-8';
    public $preserveWhiteSpace = false;
    public $namespaceURI = null;
    public $qualifiedName = '';
    public $doctype = null;
    public $formatOutput = true;
    private $xml = null;

    /**
     * Convert an Array to XML
     *
     * @param string $node_name - name of the root node to be converted
     * @param array  $arr       - aray to be converterd
     *
     * @return DomDocument
     */
    public function &createXML(&$arr = array(), $node_name = null)
    {
        $xml = $this->getXMLRoot();

        if (!is_array($arr)) {
            return $xml;
        }

        if (!empty($node_name)) {
            $xml->appendChild($this->convert($node_name, $arr));

            return $xml;
        }

        while ($row = current($arr)) {
            $xml->appendChild($this->convert(key($arr), $row));
            next($arr);
        }

        return $xml;
    }

    private function getXMLRoot()
    {
        if (empty($this->xml)) {
            $this->init();
        }

        return $this->xml;
    }

    /**
     * Initialize the root XML node [optional]
     *
     * @param $version
     * @param $encoding
     * @param $format_output
     */
    public function init()
    {
        $x = new DOMImplementation();

        if (!empty($this->namespaceURI)) {
            $dtd = $x->createDocumentType(
                $this->namespaceURI, $this->qualifiedName, $this->doctype
            );
            $dom = $x->createDocument('', '', $dtd);
        } else {
            $dom = $x->createDocument('', '');
        }

        foreach (['xmlVersion', 'encoding', 'preserveWhiteSpace'] as $k) {
            $dom->$k = $this->$k;
        }

        $this->xml = $dom;
        $this->xml->formatOutput = $this->formatOutput;
    }

    /*
     * Get the root XML node, if there isn't one, create it.
     */

    /**
     * Convert an Array to XML
     *
     * @param string $node_name - name of the root node to be converted
     * @param array  $arr       - aray to be converterd
     *
     * @return DOMNode
     */
    public function &convert($node, &$arr = array())
    {
        //print_arr($node_name);
        $xml = $this->getXMLRoot();
        if (is_string($node)) {
            $node = $xml->createElement($node);
        }

        if (is_array($arr)) {
            // get the attributes first.;
            if (isset($arr['@attributes'])) {
                foreach ($arr['@attributes'] as $key => $value) {
                    if (!$this->isValidTagName($key)) {
                        throw new Exception(
                            '[Array2XML] Illegal character in attribute name. attribute: '
                            .$key.' in node: '.$node
                        );
                    }
                    $node->setAttribute($key, $this->bool2str($value));
                }
                unset($arr['@attributes']); //remove the key from the array once done.
            }

            // check if it has a value stored in @value, if yes store the value and return
            // else check if its directly stored as string
            if (isset($arr['@value'])) {
                $node->appendChild(
                    $xml->createTextNode($this->bool2str($arr['@value']))
                );
                unset($arr['@value']);    //remove the key from the array once done.
                //return from recursion, as a note with value cannot have child nodes.
                return $node;
            } else {
                if (isset($arr['@cdata'])) {
                    $node->appendChild(
                        $xml->createCDATASection(
                            $this->bool2str($arr['@cdata'])
                        )
                    );
                    unset($arr['@cdata']);    //remove the key from the array once done.
                    //return from recursion, as a note with cdata cannot have child nodes.
                    return $node;
                }
            }
        }

        //create subnodes using recursion
        if (is_array($arr)) {
            // recurse to get the node for that key
            foreach ($arr as $key => $value) {
                if (!$this->isValidTagName($key)) {
                    throw new Exception(
                        '[Array2XML] Illegal character in tag name. tag: '.$key
                        .' in node: '.$node->name
                    );
                }
                if (is_array($value) && is_numeric(key($value))) {
                    // MORE THAN ONE NODE OF ITS KIND;
                    // if the new array is numeric index, means it is array of nodes of the same kind
                    // it should follow the parent key name
                    foreach ($value as $k => $v) {
                        $node->appendChild($this->convert($key, $v));
                    }
                } else {
                    // ONLY ONE NODE OF ITS KIND
                    $node->appendChild($this->convert($key, $value));
                }
                unset($arr[$key]); //remove the key from the array once done.
            }
        }

        // after we are done with all the keys in the array (if it is one)
        // we check if it has any text value, if yes, append it.
        if (!is_array($arr)) {
            $node->appendChild($xml->createTextNode($this->bool2str($arr)));
        }

        return $node;
    }

    /*
     * Get string representation of boolean value
     */

    private function isValidTagName($tag)
    {
        $pattern = '/^[a-z_]+[a-z0-9\:\-\.\_]*[^:]*$/i';

        return preg_match($pattern, $tag, $matches) && $matches[0] == $tag;
    }

    /*
     * Check if the tag name or attribute name contains illegal characters
     * Ref: http://www.w3.org/TR/xml/#sec-common-syn
     */

    private function bool2str($v)
    {
        //convert boolean to text value.
        $v = $v === true ? 'true' : $v;
        $v = $v === false ? 'false' : $v;

        return $v;
    }

    public function getNodeByPath($path)
    {
        $xpath = new DOMXPath($this->xml);
        $result = $xpath->query($path);
        if (!empty($result->item(0))) {
            return $result->item(0);
        }

        return null;
    }
}
