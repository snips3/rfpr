<?php

namespace App\Command;

use App\Domain\Category\Entity\Category;
use App\Domain\City\Entity\City;
use App\Domain\Product\Entity\Offer;
use App\Domain\Product\Entity\Price;
use App\Domain\Product\Entity\Product;
use App\Domain\Property\Entity\Property;
use App\Domain\Property\Entity\PropertyName;
use App\Domain\Property\Entity\PropertyValue;
use App\Domain\Shop\Entity\Shop;
use App\Domain\Category\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use JsonMachine\JsonMachine;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PhotoCodeCommand extends Command
{
    protected static $defaultName = 'p:c';

    private EntityManagerInterface $entityManager;
    private CategoryRepository $categoryRepository;
    private string $path;

    public function __construct(EntityManagerInterface $entityManager, CategoryRepository $categoryRepository)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Export Products code to output')
            // ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            // ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb
            ->select('partial product.{id, code}')
            ->from(Product::class, 'product')
            ->orderBy('product.code');

        $sql = $qb->getQuery()->getSQL();

        $result = $this->entityManager
            ->getConnection()->executeQuery($sql)->fetchAllNumeric();
        foreach ($result as $k => $v) {
            if (empty($v[1])) {
                continue;
            }
            echo $v[1], "\n";
        }

        return Command::SUCCESS;
    }
}
