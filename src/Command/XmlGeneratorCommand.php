<?php

namespace App\Command;

use App\Domain\Category\Entity\Category;
use App\Domain\City\Entity\City;
use App\Domain\Product\Entity\Offer;
use App\Domain\Product\Entity\Product;
use App\Domain\Property\Entity\Property;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class XmlGeneratorCommand extends Command
{
    use ImportTrait;

    protected static $defaultName = 'xml:generate';
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->path = implode('/', [__DIR__, '../..', 'var/cache/xml/']);
        $this->makeDir($this->path);
        chdir($this->path);
    }

    private function makeDir($path): bool
    {
        return is_dir($path) || mkdir($path, 0775, true);
    }

    protected function configure()
    {
        $this
            ->setAliases(['x:g'])
            ->setDescription('Generate *.xml files');
    }

    protected function execute(InputInterface $input, OutputInterface $output
    ): int {
        $io = new SymfonyStyle($input, $output);
        $this->createMemoryBar($output);

        $this->createXml($output);

        return Command::SUCCESS;
    }

    private function createXml(OutputInterface $output)
    {
        $msk = $this->entityManager->getRepository(City::class)->findOneBy(
            ['domain' => 'msk']
        );

        $categories = [];
        $propertyRepository = $this->entityManager->getRepository(Property::class);
        $categoryRepository = $this->entityManager->getRepository(Category::class);
        $data = $categoryRepository->findAll();
        foreach ($data as $category) {
            $categories[$category->getId()] = $category;
        }

        $offerRepository = $this->entityManager->getRepository(Offer::class);
        $data = $offerRepository->getOffersByCity($msk);
        foreach ($data as $offer) {
            /** @var Category $category */
            $category = $categories[$offer['category_id']];

            $offers[$category->getRoot()->getId()][] = $offer;
        }
        //dd(array_pop($offers));

        /*        $productIds = [];
                foreach($offers as $offer) {
                    $productIds[] = $offer['product_id'];
                }*/
        $this->trackMemory();

        /*        $productRepository = $this->entityManager->getRepository(Product::class);
                $products = $productRepository->findProductsByIds($productIds);*/
        $config = [
            'offer' => [
                '@attributes' => [
                    'available' => true,
                    'bid' => 1,
                ],
                'currencyId' => 'RUB',
                'store' => false,
                'pickup' => true,
                'sales_notes' => null,
            ],
        ];
        $arr['yml_catalog'] = [
            '@attributes' => [
                'date' => date("Y-m-d H:i"),
            ],
        ];

        $arr['yml_catalog'] = ['@attributes' => ['date' => date("Y-m-d H:i")]];
        $arr['yml_catalog']['shop'] = [
            'name' => 'RichFamily',
            'company' => 'RichFamily',
            'url' => 'https://richfamily.ru/',
            'cpa' => 1,
            'currencies' => [
                'currency' => ['@attributes' => ['id' => "RUB", 'rate' => "1"]]
            ],

            'delivery-options' => [
                'option' => ['@attributes' => ['cost' => 350, 'days' => '2 - 4', 'order-before' => '17']],

            ],
            'categories' => [],
            'offers' => [],
        ];

        $progressBar = $this->createProgressbar($output, 'Offers');
        $progressBar->setMaxSteps(count($offers));
        foreach ($offers as $categoryRootId => $offers) {
            $progressBar->advance();

            $rootCategory = $categories[$categoryRootId];

            $this->trackMemory();
            foreach ($rootCategory->getIds() as $categoryId) {
                $category = $categories[$categoryId];
                /** @var Category $category */
                $a = ['@attributes' => ['id' => $category->getId()], '@value' => trim($category->getName())];
                if ($category->getParent()) {
                    $a['@attributes']['parent_id'] = $category->getParent()->getId();
                }
                $arr['yml_catalog']['shop']['categories']['category'][] = $a;
            }

            foreach ($offers as $offer) {
                //dd($offer);
                $properties = $propertyRepository->findBy(
                    ['product' => $offer['product_id']]
                );

                $url = join(
                    '', [
                    'https://dev.rf.punicapp.com',
                    $categories[$offer['category_id']]->getUrl(),
                    '/',
                    $offer['code'],
                ]
                );
                $xmlOffer = [
                    '@attributes' => [
                        'id' => $offer['id'],
                        'available' => true,
                        'bid' => 1,
                    ],
                    'url' => $url,
                    'price' => $offer['prices'][0]['actual'],
                    'currencyId' => 'RUB',
                    'categoryId' => $offer['category_id'],

                    'store' => false,
                    'pickup' => true,
                    //'manufacturer_warranty' => true,

                    'name' => $offer['name'],

                ];

                // https://api.rf.punicapp.com/photo/194404/194404.jpg
                $xmlOffer['picture'] = 'https://api.rf.punicapp.com/photo/'.$offer['code'].'/'.$offer['code'].'.jpg';
                $xmlOffer['description'] = ['@cdata' => $offer['description']];

                foreach ($properties as $property) {
                    /** @var Property $property */
                    $xmlOffer['param'][] = [
                        '@attributes' => [
                            'name' => $property->getName()->getValue(),
                        ],
                        '@cdata' => $property->getValue()->getValue(),
                    ];
                }
 // dd($arr['yml_catalog']['shop']['categories']);
                $arr['yml_catalog']['offers'][] = $xmlOffer;
            }
            $a2x = new Array2XML;
            $a2x->namespaceURI = 'yml_catalog';
            $a2x->doctype = 'shops.dtd';
            $xml = $a2x->createXML($arr['yml_catalog'], 'yml_catalog');
            $xml->save(__DIR__.'/../../var/cache/xml/msk.'.$categoryRootId.'.xml');
        }


        return self::SUCCESS;
    }
}
