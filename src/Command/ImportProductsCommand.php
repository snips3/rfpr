<?php

namespace App\Command;

use App\Domain\Category\DTO\CategoryDTO;
use App\Domain\Category\Entity\Category;
use App\Domain\Product\DTO\OfferDTO;
use App\Domain\Product\DTO\PriceDTO;
use App\Domain\Product\DTO\ProductDTO;
use App\Domain\Product\Entity\Offer;
use App\Domain\Product\Entity\Price;
use App\Domain\Product\Entity\Product;
use App\Domain\Property\Entity\Property;
use App\Domain\Property\Entity\PropertyName;
use App\Domain\Property\Entity\PropertyValue;
use App\Domain\Shop\DTO\ShopDTO;
use App\Domain\Shop\Entity\Shop;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use JsonMachine\JsonMachine;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ImportProductsCommand extends Command
{
    use ImportTrait;

    protected static $defaultName = 'import:products';
    private EntityManagerInterface $entityManager;
    private LoggerInterface $logger;
    private SerializerInterface $serializer;
    private ValidatorInterface $validator;
    private string $path;
    private SymfonyStyle $io;

    private array $categories = [];

    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $importLogger,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->logger = $importLogger;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->path = realpath(implode('/', [__DIR__, '../..', 'var/1c']));

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Parse 1C json files & import data')
            ->addOption(
                'file',
                null,
                InputOption::VALUE_REQUIRED,
                'Choose import file',
            )
            ->setHelp(
                'This command allows Parse 1C json files & import data..., 
             use options to choose type import and action'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->createMemoryBar($output);
        $this->createProgressbar($output);

        foreach ($this->entityManager->getRepository(Category::class)->findAll() as $category) {
            /** @var Category $category */
            $this->categories[$category->getUuid()->toString()] = $category->getId();
        }

        try {
            $fileName = $input->getOption('file');
            $this->parseProducts($fileName);
        } catch (\Exception $exception) {
            $io->writeln("");
            $io->error('Error! check logs...');
            $this->logger->error($exception);

            return Command::FAILURE;
        }

        $io->writeln("");
        $io->success('Import successfully');

        return Command::SUCCESS;
    }

    private function parseProducts(string $fileName)
    {
        $colors = [];
        $related = [];
        foreach ($this->getJsonData($fileName) as $row) {
            $this->trackMemory();

            if (empty($this->categories[$row['category_id']])) {
                // $this->logger->warning("Unknown category", ['uuid' => $row['category_id'], 'product' => $row]);
                continue;
            }

            $this->createOrUpdateProduct($row);
            $this->createOrUpdateOffer($row);

            $colors[$row['id']] = $row['colors'];
            $related[$row['id']] = $row['related'];


            $this->trackMemory();
            $this->progressbar->advance();
        }//end foreach

        $this->updateRelations('products_colors', $colors);
        $this->updateRelations('products_related', $related);

        $this->progressbar->finish();
    }

    private function createOrUpdateProduct(array $product)
    {
        $arr2sql = [
            'uuid' => $product['id'],
            'category_id' => $this->categories[$product['category_id']],
            'code' => $product['code'],
        ];

        $sql = $this->createSql('products', array_keys($arr2sql));

        $stmt1 = $this->entityManager->getConnection()->prepare($sql);
        foreach ($arr2sql as $key => $value) {
            $stmt1->bindValue(':' . $key, $value);
        }

        $arr2sql = [
            'product_id' => $product['id'],
            'name' => $product['name'],
            'slug' => $product['code'],
            'article' => $product['article'],
            'description' => $product['desc'],
            'dimensions' => $product['dimensions'] && $product['dimensions'] !== 'хх' ? $product['dimensions'] : null,
            'weight' => ($product['weight'] ? $product['weight'] : null),
            'color' => ($product['color'] ? $product['color'] : null),
        ];

        $sql = $this->createSql('products_info', array_keys($arr2sql));
        $sql = str_replace(':product_id', '(SELECT `id` FROM `products` WHERE uuid = :product_id)', $sql);
        $sql = str_replace('`product_id`=VALUES(`product_id`),', '', $sql);

        $stmt2 = $this->entityManager->getConnection()->prepare($sql);
        foreach ($arr2sql as $key => $value) {
            $stmt2->bindValue(':' . $key, $value);
        }

        /**/

        $this->entityManager->beginTransaction();
        $stmt1->execute();
        $stmt2->execute();
        $this->entityManager->commit();
    }

    private function createOrUpdateOffer(array $product)
    {
        if (empty($product['offers'])) {
            return;
        }
        $sql = $this->createSql('offer', ['uuid', 'product_id', 'bar_code', 'name']);
        $sql = str_replace(':product_id', '(SELECT `id` FROM `products` WHERE uuid = :product_id)', $sql);
        $stmt = $this->entityManager->getConnection()->prepare($sql);

        foreach ($product['offers'] as $offer) {
            $stmt->bindValue(':uuid', $offer['id']);
            $stmt->bindValue(':product_id', $product['id']);
            $stmt->bindValue(':bar_code', ($offer['barcode'] ?? null));
            $stmt->bindValue(':name', ($offer['size'] ?? null));
            $stmt->execute();
        }
    }

    private function updateRelations(string $key, array $arr)
    {
/*        $this->entityManager->getConnection()
            ->getConfiguration()
            ->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger());*/
        $arr = array_filter($arr);
        $chunks = array_chunk($arr, 50, true);

        // delete old relations
        foreach ($chunks as $chunk) {
            $uuids = array_keys($chunk);
            sort($uuids);

            $sql = "DELETE FROM `$key` WHERE `product_id` IN (SELECT `id` FROM `products` WHERE `uuid` IN ('%s'))";
            $sql = sprintf($sql, implode("','", $uuids));
            $stmt = $this->entityManager->getConnection()->prepare($sql);

            $stmt->execute();
        }

        //create new relations
        foreach ($chunks as $chunk) {
            $uuids = array_keys($chunk);
            sort($uuids);

            foreach ($chunk as $uuid => $related) {
                foreach ($related as $id) {
                    $sql = "INSERT IGNORE INTO `$key` (`id`, `product_id`) VALUES ((SELECT id FROM products WHERE uuid='%s'),(SELECT id FROM products WHERE uuid='%s'))";
                    $sql = sprintf($sql, $uuid, $id);

                    $stmt = $this->entityManager->getConnection()->prepare($sql);
                    $stmt->execute();
                }
            }
        }
    }
}
