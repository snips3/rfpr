<?php

namespace App\Command;

use JsonMachine\JsonMachine;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

trait ImportTrait
{
    protected ProgressBar $memoryBar;
    protected ProgressBar $progressbar;

    protected function createMemoryBar(OutputInterface $output): ProgressBar
    {
        ini_set('memory_limit', '2048M');

        /** @var OutputInterface $section */
        $section = $output->section('* Memory Usage');
        $section->writeln('<info>* Memory Usage</info>');

        $this->memoryBar = new ProgressBar($section, 2048);
        $this->memoryBar->setEmptyBarCharacter('░');
        $this->memoryBar->setProgressCharacter('');
        $this->memoryBar->setBarCharacter('▓');

        return $this->memoryBar;
    }

    private function trackMemory(): void
    {
        $this->memoryBar->setProgress(
            ((int) memory_get_peak_usage(true) / 1024 / 1024)
        );
    }

    protected function createProgressbar(OutputInterface $output, string $name = 'Products'): ProgressBar
    {
        $section = $output->section($name);
        $section->writeln('');
        $section->writeln('<comment>* Products</comment>');

        $this->progressbar = new ProgressBar($section);
        $this->progressbar->setEmptyBarCharacter('░');
        $this->progressbar->setProgressCharacter('');
        $this->progressbar->setBarCharacter('▓');

        return $this->progressbar;
    }

    private function createSql(string $table, array $data): string
    {
        return sprintf(
            "INSERT IGNORE INTO `%s` (%s) VALUES (%s) ON DUPLICATE KEY UPDATE %s",
            $table,
            '`' . implode('`, `', $data) . '`',
            ':' . implode(', :', $data) . '',
            self::arr2upd($data)
        );
    }

    private function arr2upd(array $arr, ?string $delimiter = ','): string
    {
        $str = [];
        if (is_array($arr)) {
            //$arr = array_filter($arr);
            foreach ($arr as $key) {
                $str[] = "`" . $key . "`=VALUES(`" . $key . "`)";
            }
        }
        $str = implode($delimiter, $str);

        return $str;
    }

    function StrCaseCmp($str1, $str2, $encoding = null): int
    {
        if (null === $encoding) {
            $encoding = mb_internal_encoding();
        }

        $str1 = trim(str_replace('ё', 'е', $str1));
        $str2 = trim(str_replace('ё', 'е', $str2));

        return strcmp(
            mb_strtoupper($str1, $encoding),
            mb_strtoupper($str2, $encoding)
        );
    }

    private function getJsonData(string $fileName): JsonMachine
    {
        return JsonMachine::fromFile($this->path . '/' . $fileName);
    }
}
