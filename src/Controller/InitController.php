<?php

namespace App\Controller;

use App\Domain\Category\Repository\CategoryRepository;
use App\Domain\City\Repository\CityRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(name="api_init_", path="/init")
 */
class InitController extends ApiController
{
    /**
     * @Route("", name="init", methods={"GET"})
     */
    public function index(CategoryRepository $categoryRepository, CityRepository $cityRepository): JsonResponse
    {
        return $this->json([
            'data' => [
                'cities' => $cityRepository->findAll(),
                'categories' => $categoryRepository->findBy(['active' => true]),
            ],
        ]);
    }
}
