<?php

namespace App\Controller\Customer;

use App\Controller\ApiController;
use App\Domain\Customer\DTO\CustomerRegistrationDTO;
use App\Domain\Customer\Entity\Customer;
use App\Domain\Customer\Service\RegistrationService;
use App\Domain\Sms\Service\SmsSenderInterface;
use App\Utility\Phone\Phone;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationSuccessResponse;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route(name="api_customer_", path="/customer")
 */
class AuthController extends ApiController
{
    private RegistrationService $registrationService;

    public function __construct(RegistrationService $registrationService)
    {
        $this->registrationService = $registrationService;
    }

    /**
     * @Route("/auth", name="auth", methods={"POST"})
     * @Security("is_granted('IS_AUTHENTICATED_ANONYMOUSLY')")
     * @ParamConverter(name="customer", class="App\Domain\Customer\Entity\Customer", converter="auth.param_converter")
     */
    public function auth(Customer $customer, JWTTokenManagerInterface $manager): Response
    {
        $jwt = $manager->createFromPayload($customer, []);

        return new JWTAuthenticationSuccessResponse($jwt);
    }

    /**
     * @Route("/sms", name="sms", methods={"POST"})
     * @Security("is_granted('IS_AUTHENTICATED_ANONYMOUSLY')")
     * @ParamConverter(name="customer", class="App\Domain\Customer\Entity\Customer", converter="phone.param_converter")
     */
    public function sms(?Customer $customer, ?Phone $phone, SmsSenderInterface $smsSender): Response
    {
        if ($customer === null) {
            $customerDTO = new CustomerRegistrationDTO();
            $customerDTO->phone = $phone;
            $customer = $this->registrationService->register($customerDTO);
        }

        $smsSender->sendSmsFor($customer);

        return $this->json([], Response::HTTP_NO_CONTENT);
    }
}
