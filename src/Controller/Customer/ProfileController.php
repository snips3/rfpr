<?php

namespace App\Controller\Customer;

use App\Controller\ApiController;
use App\Domain\Order\Entity\Order;
use App\Domain\Order\Service\OrderService;
use App\Domain\Product\Entity\Product;
use App\Modules\Payment\PaymentGatewayInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route(name="api_customer_", path="/customer")
 */
class ProfileController extends ApiController
{
    private EntityManagerInterface $entityManager;
    private OrderService $orderService;

    public function __construct(EntityManagerInterface $entityManager, OrderService $orderService)
    {
        $this->entityManager = $entityManager;
        $this->orderService = $orderService;
    }

    /**
     * @Route("", name="profile", methods={"GET"})
     */
    public function profile()
    {
        $customer = $this->getUser();

        return $this->json([
            'data' => [
                'name'        => $customer->getName(),
                'phone'       => $customer->getPhone()->getValue(),
                'favorites'   => $customer->getFavorites()->getIds(),
                'comparisons' => $customer->getComparisons()->getIds(),
                'cart'        => $customer->getCartItems(),
            ],
        ]);
    }

    /**
     * @Route("/order", name="order", methods={"POST"})
     * @ParamConverter(name="order", class="App\Domain\Order\Entity\Order", converter="order_dto.param_converter")
     */
    public function order(Order $order, PaymentGatewayInterface $gateway)
    {
        $this->orderService->addOrder($order);

        $response = $gateway->register($order);

        return $this->json([
            'data' => $response],
            Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/favorite/{id}", name="favorite", methods={"POST"})
     */
    public function favorite(Product $product)
    {
        $customer = $this->getUser();

        $customer->addFavorite($product);

        $this->entityManager->persist($customer);
        $this->entityManager->flush();

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/favorite/{id}", name="unfavorite", methods={"DELETE"})
     */
    public function unfavorite(Product $product)
    {
        $customer = $this->getUser();

        $customer->removeFavorite($product);

        $this->entityManager->persist($customer);
        $this->entityManager->flush();

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/compare/{id}", name="compare", methods={"POST"})
     */
    public function compare(Product $product)
    {
        $customer = $this->getUser();

        $customer->addComparison($product);

        $this->entityManager->persist($customer);
        $this->entityManager->flush();

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/compare/{id}", name="uncompare", methods={"DELETE"})
     */
    public function uncompare(Product $product)
    {
        $customer = $this->getUser();

        $customer->removeComparison($product);

        $this->entityManager->persist($customer);
        $this->entityManager->flush();

        return $this->json([], Response::HTTP_NO_CONTENT);
    }
}
