<?php

namespace App\Controller\Customer;

use App\Controller\ApiController;
use App\Domain\CartItem\Entity\CartItem;
use App\Domain\CartItem\Service\CartItemService;
use App\Domain\Product\Entity\Offer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;

/**
 * @Route(name="api_customer_cart_", path="/customer/cart")
 */
class CartController extends ApiController
{
    private CartItemService $cartItemService;
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager, CartItemService $cartItemService)
    {
        $this->cartItemService = $cartItemService;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("", name="add", methods={"POST"})
     * @ParamConverter("offer", class=App\Domain\Product\Entity\Offer::class)
     */
    public function add(Offer $offer)
    {
        $customer = $this->getUser();

        $this->cartItemService->add($customer, $offer);

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/{offer_id}", name="edit", methods={"PATCH"})
     * @ParamConverter("cartItem", class=App\Domain\CartItem\Entity\CartItem::class)
     */
    public function editCount(Request $request, CartItem $cartItem)
    {
        $customer = $this->getUser();
        ["count" => $count] = json_decode($request->getContent(), true);
        $this->cartItemService->editCount($customer, $cartItem, $count);

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("", name="delete_all", methods={"DELETE"})
     * @Route("/{offer_id}", name="delete", methods={"DELETE"})
     * @Entity("cartItem", expr="repository.findByCartItem(offer_id)")
     */
    public function delete(?CartItem $cartItem)
    {
        $customer = $this->getUser();
        if ($cartItem) {
            $customer->removeCartItem($cartItem);
        } else {
            $customer->removeCartItems();
        }

        $this->entityManager->flush();

        return $this->json([], Response::HTTP_NO_CONTENT);
    }
}
