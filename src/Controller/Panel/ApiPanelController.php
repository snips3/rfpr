<?php

namespace App\Controller\Panel;

use App\Controller\ApiController;
use Doctrine\ORM\EntityManagerInterface;

class ApiPanelController extends ApiController
{
    protected EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
}
