<?php

namespace App\Controller\Panel;

use App\Domain\City\Entity\City;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @Route(name="api_panel_city_", path="/panel/city")
 */
class CityController extends ApiPanelController
{
    /**
     * @Route("/list", name="list", methods={"GET"})
     */
    public function list()
    {
        return $this->json([
            'data' => $this->entityManager->getRepository(City::class)->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="city", requirements={"id"="\d+"}), methods={"GET"})
     */
    public function city(City $city)
    {
        return $this->json(['data' => $city]);
    }

    /**
     * @Route("", name="post", requirements={"id"="\d+"}), methods={"POST"})
     */
    public function post(Request $request)
    {
        $jsonData = $request->getContent();

        $city = $this->serializer->deserialize(
            $jsonData,
            City::class,
            'json',
        );

        $this->entityManager->persist($city);
        $this->entityManager->flush();

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/{id}", name="patch", requirements={"id"="\d+"}), methods={"PATCH"})
     */
    public function patch(City $city, Request $request)
    {
        $jsonData = $request->getContent();

        $this->serializer->deserialize(
            $jsonData,
            City::class,
            'json',
            [AbstractNormalizer::OBJECT_TO_POPULATE => $city]
        );

        $this->entityManager->flush();

        return $this->json([], Response::HTTP_NO_CONTENT);
    }
}
