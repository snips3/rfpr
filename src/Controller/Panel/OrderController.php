<?php

namespace App\Controller\Panel;

use App\Domain\Order\Entity\Order;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @Route(name="api_panel_order_", path="/panel/order")
 */
class OrderController extends ApiPanelController
{
    /**
     * @Route("/list", name="list", methods={"GET"})
     */
    public function list()
    {
        return $this->json([
            'data' => $this->entityManager->getRepository(Order::class)->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="order", requirements={"id"="\d+"}), methods={"GET"})
     */
    public function order(Order $order)
    {
        return $this->json([
            'data' => $order,
        ]);
    }

    /**
     * @Route("/{id}", name="patch", requirements={"id"="\d+"}), methods={"PATCH"})
     */
    public function patch(Order $order, Request $request)
    {
        $jsonData = $request->getContent();

        $this->serializer->deserialize(
            $jsonData,
            Order::class,
            'json',
            [AbstractNormalizer::OBJECT_TO_POPULATE => $order]
        );

        $this->entityManager->flush();

        return $this->json([], Response::HTTP_NO_CONTENT);
    }
}