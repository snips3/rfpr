<?php

namespace App\Controller\Panel;

use App\Domain\Category\Entity\Category;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @Route(name="api_panel_category_", path="/panel/category")
 */
class CategoryController extends ApiPanelController
{
    /**
     * @Route("/list", name="list", methods={"GET"})
     */
    public function list(): JsonResponse
    {
        $data = [];
        $categories = $this->entityManager->getRepository(Category::class)->findAll();
        foreach ($categories as $category) {
            /** @var Category $category */
            $data[] = array_merge($category->jsonSerialize(), ['properties' => $category->getPropertyIds()]);
        }
        return $this->json(['data' => $data]);
    }

    /**
     * @Route("/{id}", name="category", requirements={"id"="\d+"}), methods={"GET"})
     */
    public function category(Category $category)
    {
        return $this->json(['data' => $category]);
    }

    /**
     * @Route("/{category}/products", name="products", methods={"GET"}, requirements={"category"="\d+"})
     */
    public function products(Category $category)
    {
        $products = $category->getProducts();

        return $this->json([
            'total' => $products->count(),
            'data' => $products->toArray(),
            'prices' => $products->getMinMaxPrices(),
        ]);
    }

    /**
     * @Route("/{id}", name="patch", requirements={"id"="\d+"}), methods={"PATCH"})
     */
    public function patch(Category $category, Request $request): JsonResponse
    {
        $jsonData = $request->getContent();

        $this->serializer->deserialize(
            $jsonData,
            Category::class,
            'json',
            [AbstractNormalizer::OBJECT_TO_POPULATE => $category]
        );

        $this->entityManager->flush();

        return $this->json([], Response::HTTP_NO_CONTENT);
    }
}
