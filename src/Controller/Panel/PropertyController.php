<?php

namespace App\Controller\Panel;

use App\Domain\Property\Entity\PropertyCname;
use App\Domain\Property\Entity\PropertyName;
use App\Domain\Property\Entity\PropertyValue;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(name="api_panel_property_name_", path="/panel/property")
 */
class PropertyController extends ApiPanelController
{
    /**
     * @Route("/list", name="list", methods={"GET"})
     */
    public function list(): JsonResponse
    {
        return $this->json([
            'data' => [
                'name' => $this->entityManager->getRepository(PropertyName::class)->findAll(),
                'value' => $this->entityManager->getRepository(PropertyValue::class)->findAll(),
                'cname' => $this->entityManager->getRepository(PropertyCname::class)->findAll(),
            ],
        ]);
    }
}
