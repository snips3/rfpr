<?php

namespace App\Controller\Panel;

use App\Domain\Product\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

/**
 * @Route(name="api_panel_product_", path="/panel/product")
 */
class ProductController extends ApiPanelController
{
    /**
     * @Route("/list", name="list", methods={"GET"})
     */
    public function list()
    {
        return $this->json([
            'data' => $this->entityManager->getRepository(Product::class)->findAll()->toArray(),
        ]);
    }

    /**
     * @Route("/{id}", name="product", requirements={"id"="\d+"}), methods={"GET"})
     */
    public function product(Product $product)
    {
        return $this->json([
            'data' => $product,
        ]);
    }

    /**
     * @Route("/{id}", name="patch", requirements={"id"="\d+"}), methods={"PATCH"})
     */
    public function patch(Product $product, Request $request)
    {
        $jsonData = $request->getContent();

        $this->serializer->deserialize(
            $jsonData,
            Product::class,
            'json',[
            AbstractNormalizer::OBJECT_TO_POPULATE => $product,
            AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
            ],
        );

        $this->entityManager->flush();

        return $this->json([], Response::HTTP_NO_CONTENT);
    }
}
