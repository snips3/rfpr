<?php

namespace App\Controller\Panel;

use App\Domain\Shop\Entity\Shop;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @Route(name="api_panel_shop_", path="/panel/shop")
 */
class ShopController extends ApiPanelController
{
    /**
     * @Route("/list", name="list", methods={"GET"})
     */
    public function list()
    {
        return $this->json([
            'data' => $this->entityManager->getRepository(Shop::class)->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="shop", requirements={"id"="\d+"}), methods={"GET"})
     */
    public function shop(Shop $shop)
    {
        return $this->json(['data' => $shop]);
    }

    /**
     * @Route("/{id}", name="patch", requirements={"id"="\d+"}), methods={"PATCH"})
     */
    public function patch(Shop $shop, Request $request)
    {
        $jsonData = $request->getContent();

        $this->serializer->deserialize(
            $jsonData,
            Shop::class,
            'json',
            [AbstractNormalizer::OBJECT_TO_POPULATE => $shop]
        );

        $this->entityManager->flush();

        return $this->json([], Response::HTTP_NO_CONTENT);
    }
}
