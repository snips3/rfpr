<?php

namespace App\Controller;

use App\Domain\Category\Entity\Category;
use App\Domain\Category\Repository\CategoryRepository;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(name="api_category_", path="/category")
 */
class CategoryController extends ApiController
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/list", name="list", methods={"GET"})
     */
    public function list()
    {
        return $this->json([
            'data' => $this->categoryRepository->findBy(['active' => true]),
        ]);
    }

    /**
     * @Route("/{category}/products", name="products", methods={"GET"}, requirements={"category"="\d+"})
     */
    public function products(Category $category)
    {
        $products = $category->getProducts();

        return $this->json([
            'total' => $products->count(),
            'data' => $products->toArray(),
            'prices' => $products->getMinMaxPrices(),
        ]);
    }

    /**
     * @Route("/{category}/filters", name="filters", methods={"GET"}, requirements={"category"="\d+"})
     */
    public function filters(Category $category)
    {
        return $this->json(['data' => $this->categoryRepository->getFiltersForCategory($category)]);
    }

    /**
     * @Route("/{category}/sorting", name="sorting", methods={"GET"}, requirements={"category"="\d+"})
     */
    public function sorting(Category $category)
    {
        $productsIds = $category->getProducts()->getIds();

        shuffle($productsIds);
        $sorted['pop'] = $productsIds;
        shuffle($productsIds);
        $sorted['price'] = $productsIds;
        shuffle($productsIds);
        $sorted['new'] = $productsIds;

        return $this->json([
            'data' => $sorted
        ]);
    }
}
