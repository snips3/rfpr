<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(name="api_1c_", path="/1c")
 */
class C1Controller extends ApiController
{
    const PRICES = 'prices.json';
    const PRODUCTS = 'products.json';
    const OFFERS = 'offers.json';
    const STOCKS = 'stocks.json';
    const PROPERTIES = 'properties.json';
    const ACTUAL = 'actual.json';

    /**
     * @Route("/shop", name="shop", methods={"POST"})
     * @Route("/category", name="category", methods={"POST"})
     * @Route("/price", name="price", methods={"POST"})
     * @Route("/stock", name="stock", methods={"POST"})
     * @Route("/property", name="property", methods={"POST"})
     * @Route("/product", name="product", methods={"POST"})
     * @Route("/actual", name="actual", methods={"POST"})
     */
    public function __invoke(RequestStack $requestStack): JsonResponse
    {
        $parts = explode('_', $requestStack->getCurrentRequest()->get('_route'));
        $filename = sprintf(
            '/var/www/html/var/1c/%s_%s.json',
            array_pop($parts),
            date('YmdHis')
        );

        file_put_contents($filename, $requestStack->getCurrentRequest()->getContent());

        return $this->json(['result' => 'ok'], Response::HTTP_OK);
    }
}
