<?php

namespace App\Controller;

use App\Domain\City\Entity\City;
use App\Domain\Product\Repository\OfferRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(name="api_search_", path="/search")
 */
class SearchController extends ApiController
{
    private OfferRepository $offerRepository;

    public function __construct(OfferRepository $offerRepository)
    {
        $this->offerRepository = $offerRepository;
    }

    /**
     * @Route("/", name="product", methods={"GET"})
     * @ParamConverter(name="city", class="App\Domain\City\Entity\City", converter="city.param_converter")
     */
    public function products(City $city, Request $request): JsonResponse
    {
        $query = $request->query->get('q');

        if (empty($query) || !is_string($query)) {
            return $this->json(['data' => []]);
        }

        $products = $this->offerRepository
            ->getProductsWithOffersByCityAndQuery($city, $query);

        return $this->json(
            [
                'total' => count($products),
                'data' => $products,
            ]
        );
    }
}
