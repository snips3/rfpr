<?php

namespace App\Controller;

use App\Domain\City\Entity\City;
use App\Domain\Product\Entity\Offer;
use App\Domain\Product\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(name="api_product_", path="/product")
 */
class ProductController extends ApiController
{
    /**
     * @Route("/{product}", name="product", methods={"GET"}, requirements={"product"="\d+"})
     */
    public function product(City $city, Product $product, EntityManagerInterface $entityManager): JsonResponse
    {
        $entityManager
            ->getFilters()
            ->enable('city')
            ->setParameter('shop_ids', implode(',', $city->getShops()->getIds()));

        return $this->json([
            'data' => array_merge(
                $product->jsonSerialize(),
                [
                    'offers' => $product->getOffers(),
                    'colors' => $product->getColors()->getIds(),
                    'related' => $product->getRelated()->getIds(),
                ]
            ),
        ]);
    }
}
