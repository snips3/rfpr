<?php

namespace App\EventListener;

use App\Exception\ApiException;
use App\Exception\ApiExceptionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiExceptionListener
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $apiLogger)
    {
        $this->logger = $apiLogger;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        // Customize your response object to display the exception details

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $response = new JsonResponse([
                'error' => [
//                    'code' => $exception->getCode(),
                    'status_code' => $exception->getStatusCode(),
                    'message' => $exception->getMessage(),
                ],
            ]);
            $response->setStatusCode($exception->getStatusCode());
        } else {
            $response = new JsonResponse([
                'error' => [
//                    'code' => $exception->getCode(),
                    'status_code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                    'message' => $exception->getMessage(),
                ],
            ]);
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        if (!($exception instanceof NotFoundHttpException)) {
            $this->logger->error(
                $exception->getMessage(),
                [
                    'exception' => $exception,
                    'payload' => $event->getRequest()->getContent(),
                ]
            );
        }
        // sends the modified response object to the event

        $event->setResponse($response);
    }
}
