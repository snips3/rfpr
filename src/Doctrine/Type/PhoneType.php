<?php

namespace App\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use App\Utility\Phone\Phone;

class PhoneType extends Type
{
    private const TYPE_NAME = 'phone';

    public function getName(): string
    {
        return self::TYPE_NAME;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToPHPValue($phoneAsString, AbstractPlatform $platform): ?Phone
    {
        if (empty($phoneAsString)) {
            return null;
        }

        return new Phone($phoneAsString);
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}
