<?php

namespace App\Doctrine\Filter;

use App\Domain\Product\Entity\Offer;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class CityFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        if ($targetEntity->reflClass->getName() !== Offer::class) {
            return "";
        }
        $ids = substr($this->getParameter('shop_ids'), 1, -1);

        return sprintf('shop_id IN (%s)', $ids);
    }
}
