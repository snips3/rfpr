<?php

namespace App\Normalizer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;

class CollectionNormalizer extends ArrayDenormalizer
{
    public function supportsDenormalization($data, string $type, string $format = null, array $context = []): bool
    {
        return strpos($type, 'App\\Domain\\') === 0
            && strpos($type, '[]') !== false
            && is_array($data);
    }

    public function denormalize(
        $data,
        string $type,
        string $format = null,
        array $context = []
    ) {
        return new ArrayCollection(parent::denormalize(
            $data,
            $type,
            $format,
            $context
        ));
    }
}
