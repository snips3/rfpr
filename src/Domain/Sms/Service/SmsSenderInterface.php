<?php

namespace App\Domain\Sms\Service;

use App\Domain\Customer\Entity\Customer;
use App\Domain\Sms\Entity\Sms;
use App\Modules\Sms\Provider\SmsProviderInterface;

interface SmsSenderInterface
{
    public function sendSmsFor(Customer $customer);
}
