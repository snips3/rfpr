<?php

namespace App\Domain\Sms\Service;

use App\Domain\Customer\Entity\Customer;
use App\Domain\Sms\Entity\Sms;
use App\Domain\Sms\Entity\SmsCode;
use App\Domain\Sms\Repository\SmsCodeRepository;
use App\Domain\Sms\Repository\SmsRepository;
use App\Modules\Sms\Provider\SmsProviderInterface;
use Doctrine\ORM\EntityManagerInterface;

class SmsCodeService implements SmsSenderInterface
{
    private EntityManagerInterface $entityManager;
    private SmsProviderInterface $smsProvider;
    private SmsCodeRepository $smsRepository;

    public function __construct(EntityManagerInterface $entityManager, SmsProviderInterface $smsProvider, SmsCodeRepository $smsRepository)
    {
        $this->entityManager = $entityManager;
        $this->smsProvider = $smsProvider;
        $this->smsRepository = $smsRepository;
    }

    public function sendSmsFor(Customer $customer)
    {
        $sms = $this->createSmsFor($customer);

        return $this->smsProvider->send($sms);
    }

    private function createSmsFor(Customer $customer): Sms
    {
        array_map(fn($sms) => $sms->setActive(false), $this->smsRepository->getAllForPhone($customer->getPhone()));

        $sms = new SmsCode($customer->getPhone());

        $this->entityManager->persist($sms);
        $this->entityManager->flush();

        return $sms;
    }
}
