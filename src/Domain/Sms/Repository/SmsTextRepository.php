<?php

namespace App\Domain\Sms\Repository;

use App\Domain\Sms\Entity\Sms;
use App\Domain\Sms\Entity\SmsCode;
use App\Domain\Sms\Entity\SmsText;
use App\Utility\Phone\Phone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class SmsTextRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SmsText::class);
    }

    public function getAllForPhone(Phone $phone)
    {
        return $this->findBy(['phone' => $phone]);
    }
}
