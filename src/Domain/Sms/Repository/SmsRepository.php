<?php

namespace App\Domain\Sms\Repository;

use App\Domain\Sms\Entity\Sms;
use App\Domain\Sms\Entity\SmsCode;
use App\Domain\Sms\Entity\SmsText;
use App\Utility\Phone\Phone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sms|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sms|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sms[]    findAll()
 * @method Sms[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SmsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sms::class);
    }

    public function getActiveSmsCode(Phone $phone)
    {
        $sms = $this->_em->createQueryBuilder()
            ->select('sms')
            ->from(SmsCode::class, 'sms')
            ->andWhere('u.phone = ?1')
            ->andWhere('u.isActive = ?2')
            ->setParameter(1, $phone)
            ->setParameter(2, true)
            ->getQuery()
            ->getOneOrNullResult();

        return $sms;
    }

    public function getAllSmsText(Phone $phone)
    {
        $smses = $this->_em->createQueryBuilder()
            ->select('u')
            ->from(SmsText::class, 'u')
            ->andWhere('u.phone = ?1')
            ->setParameter(1, $phone)
            ->getQuery()
            ->getResult();

        return $smses;
    }
}
