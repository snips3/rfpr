<?php

namespace App\Domain\Sms\Entity;

use App\Domain\Customer\Entity\Customer;
use App\Domain\Sms\Repository\SmsRepository;
use App\Utility\Phone\Phone;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=SmsRepository::class)
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "code":"App\Domain\Sms\Entity\SmsCode",
 *     "text":"App\Domain\Sms\Entity\SmsText",
 * })
 */
abstract class Sms implements SmsInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="phone", length=255)
     */
    private Phone $phone;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Timestampable(on="create")
     */
    private \DateTimeInterface $createdAt;

    public function __construct(Phone $phone)
    {
        $this->setPhone($phone);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPhone(): Phone
    {
        return $this->phone;
    }

    public function setPhone(Phone $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }
}
