<?php

namespace App\Domain\Sms\Entity;

use App\Utility\Phone\Phone;

interface SmsInterface
{
    public function getId(): int;
    public function getPhone(): Phone;
    public function getValue(): string;
}
