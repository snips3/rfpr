<?php

namespace App\Domain\Sms\Entity;

use App\Utility\Phone\Phone;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SmsCodeRepository::class)
 */
class SmsCode extends Sms
{
    /**
     * @ORM\Column(type="string", length=4)
     */
    private string $value;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private bool $isActive = true;

    public function __construct(Phone $phone)
    {
        parent::__construct($phone);

        $code = random_int(1000, 9999);
        $this->setValue($code);
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function isActive(): bool
    {
        return (bool) $this->isActive;
    }

    public function setActive(bool $active): Sms
    {
        $this->isActive = $active;

        return $this;
    }
}
