<?php

namespace App\Domain\Sms\Entity;

use App\Utility\Phone\Phone;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class SmsText extends Sms
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $value;

    public function __construct(Phone $phone)
    {
        parent::__construct($phone);
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
