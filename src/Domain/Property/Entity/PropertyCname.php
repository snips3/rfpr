<?php

namespace App\Domain\Property\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(
 *     indexes={@ORM\Index(name="filters", columns={"property_name_id", "property_value_id", "synonym"})},
 *     uniqueConstraints={@ORM\UniqueConstraint(columns={"property_name_id", "property_value_id"})})
 */
class PropertyCname implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=PropertyName::class,  cascade={"persist"})
     * @ORM\JoinColumn(name="property_name_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private PropertyName $name;

    /**
     * @ORM\ManyToOne(targetEntity=PropertyValue::class, cascade={"persist"})
     * @ORM\JoinColumn(name="property_value_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private PropertyValue $value;

    /**
     * @ORM\Column(type="string")
     */
    private string $synonym;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?PropertyName
    {
        return $this->name;
    }

    public function getValue(): ?PropertyValue
    {
        return $this->value;
    }

    public function getSynonym(): string
    {
        return $this->synonym;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName()->getId(),
            'value' => $this->getValue()->getId(),
            'cname' => $this->getSynonym(),
        ];
    }
}
