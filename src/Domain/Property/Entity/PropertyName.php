<?php

namespace App\Domain\Property\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass="App\Domain\Property\Repository\PropertyNameRepository")
 * @ORM\Table(indexes={@ORM\Index(name="filters", columns={"filter", "id"})})
 */
class PropertyName implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private string $value;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private ?string $cname = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?bool $filter = null;

    /**
     * @ORM\Column(type="string", options={"default": "checkbox"})
     */
    private string $type = 'checkbox';

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }

    public function isFilter(): bool
    {
        return (bool) $this->filter;
    }

    public function getColumns(): int
    {
        return (int) $this->filter;
    }

    public function getCname(): ?string
    {
        return $this->cname;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'value' => $this->getValue(),
        ];
    }
}
