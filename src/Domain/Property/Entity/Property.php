<?php

namespace App\Domain\Property\Entity;

use App\Domain\Product\Entity\Offer;
use App\Domain\Product\Entity\Product;
use App\Domain\Property\Repository\PropertyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PropertyRepository::class)
 *
 */
class Property
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=PropertyName::class,  cascade={"persist"})
     * @ORM\JoinColumn(name="property_name_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private PropertyName $name;

    /**
     * @ORM\ManyToOne(targetEntity=PropertyValue::class, cascade={"persist"})
     * @ORM\JoinColumn(name="property_value_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private PropertyValue $value;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Product\Entity\Product", inversedBy="properties")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?Product $product = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Product\Entity\Offer", inversedBy="properties")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?Offer $offer = null;

    public function __construct(PropertyName $name, PropertyValue $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): PropertyName
    {
        return $this->name;
    }

    public function setName(PropertyName $name): void
    {
        $this->name = $name;
    }

    public function getValue(): PropertyValue
    {
        return $this->value;
    }

    public function setValue(PropertyValue $value): void
    {
        $this->value = $value;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    public function setOffer(?Offer $offer): void
    {
        $this->offer = $offer;
    }
}
