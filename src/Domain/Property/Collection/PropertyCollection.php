<?php

namespace App\Domain\Property\Collection;

use App\Domain\Property\Entity\Property;
use Doctrine\Common\Collections\ArrayCollection;
use JsonSerializable;

class PropertyCollection extends ArrayCollection implements JsonSerializable
{
    public function getAssoc(): array
    {
        return $this->map(function (Property $property) {
            return [
                'name' => $property->getName()->__toString(),
                'value' => $property->getValue()->__toString(),
            ];
        })->toArray();
    }

    public function jsonSerialize(): array
    {
        return $this->getAssoc();
    }

}
