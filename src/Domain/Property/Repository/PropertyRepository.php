<?php

namespace App\Domain\Property\Repository;

use App\Domain\Category\Entity\Category;
use App\Domain\City\Entity\City;
use App\Domain\Product\Entity\Offer;
use App\Domain\Property\Entity\Property;
use App\Domain\Property\Entity\PropertyCname;
use App\Domain\Property\Entity\PropertyName;
use App\Domain\Property\Entity\PropertyValue;
use App\Traits\QueryBuilderTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\DocBlock\Tags\Method;

/**
 * @method Property|null find($id, $lockMode = null, $lockVersion = null)
 * @method Property|null findOneBy(array $criteria, array $orderBy = null)
 * @method Property[]    findAll()
 * @method Property[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyRepository extends ServiceEntityRepository
{
    use QueryBuilderTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Property::class);
    }

    public function getFiltersBy(City $city, Category $category): array
    {
        $offers = $this->getEntityManager()
            ->getRepository(Offer::class)
            ->getOffersAndProductsIdsBy($city, $category);

        $offersIds = array_keys($offers);
        $productsIds = array_values($offers);
        sort($productsIds);
        $productsIds = array_unique($productsIds);

        return $this->getFiltersDataBy($productsIds, $offersIds);
    }

    public function getFiltersDataBy(
        array $productsIds = [], array $offersIds = []
    ): array {
        if (empty($productsIds)) {
            $productsIds = [0];
        }
        if (empty($offersIds)) {
            $offersIds = [0];
        }
        $filters = [];
        $properties = [];
        $propertyValuesIds = [];

        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('property')
            ->from(Property::class, 'property')
            ->join(
                PropertyName::class,
                'property_name',
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->isNotNull('property_name.filter'),
                    $qb->expr()->eq('property_name.id', 'property.name')
                )
            )
            // ->setMaxResults(100)
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->in('property.product', $productsIds),
                    $qb->expr()->in('property.offer', $offersIds),
                )
            );

        $sql = $qb->getQuery()->getSQL();

        $result = $this->getEntityManager()->getConnection()
            ->fetchAllAssociative($sql);

        $cnt = count($result);
        for ($i = 0; $i < $cnt; $i++) {
            $row = array_map('intval', $result[$i]);
            $row = $this->clearKeysRow($row);
            unset($result[$i]);

            $propertiesIds[] = $row['id'];
            $properties[$row['property_name_id']][] = $row['property_value_id'];
            $propertyValuesIds[] = $row['property_value_id'];

            if ($row['product_id']) {
                $filters['products'][$row['product_id']][$row['property_name_id']][]
                    = $row['property_value_id'];
            }
            if ($row['offer_id']) {
                $filters['offers'][$row['offer_id']][$row['property_name_id']][]
                    = $row['property_value_id'];
            }
        }

        array_walk($properties, 'sort');
        $properties = array_map('array_unique', $properties);

        $filters['names'] = $this->getPropertyNamesByIds($properties);
        $filters['values'] = $this->getPropertyValuesByIds($propertyValuesIds);
        $filters['cname'] = $this->getPropertyCnames($propertiesIds ?? []);

        return $filters;
    }

    private function getPropertyNamesByIds(array $properties): array
    {
        $types = ['checkbox' => 1, 'colorbox' => 2, 'range' => 3];
        $ids = array_keys($properties);
        sort($ids);
        $ids = array_unique($ids);

        if (empty($ids)) {
            return [];
        }
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->from(PropertyName::class, 'data')
            ->select('data')
            ->where($qb->expr()->in('data.id', $ids));

        $sql = $qb->getQuery()->getSQL();
        $result = $this->getEntityManager()->getConnection()->executeQuery($sql)
            ->fetchAllNumeric();

        $data = [];
        $resultLength = count($result);
        for ($i = 0; $i < $resultLength; $i++) {
            $row = array_values($result[$i]);
            $data[$row[0]] = [
                'title' => $row[2] ?? $row[1],
                'cols' => (int)$row[3],
                'type' => $types[$row[4]],
                'values' => array_values($properties[$row[0]]),
            ];
            unset($result[$i]);
        }

        return $data;
    }

    private function getPropertyValuesByIds(array $ids): array
    {
        sort($ids);
        $ids = array_unique($ids);

        if (empty($ids)) {
            return [];
        }
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->from(PropertyValue::class, 'data')
            ->select('data')
            ->where($qb->expr()->in('data.id', $ids));

        $sql = $qb->getQuery()->getSQL();
        $result = $this->getEntityManager()->getConnection()->executeQuery($sql)
            ->fetchAllNumeric();

        $data = [];
        $resultLength = count($result);
        for ($i = 0; $i < $resultLength; $i++) {
            $row = array_values($result[$i]);
            $data[$row[0]] = $row[1];
            unset($result[$i]);
        }

        return $data;
    }

    private function getPropertyCnames(array $propertiesIds): array
    {
        if (empty($propertiesIds)) {
            return [];
        }
        sort($propertiesIds);
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('cname')
            ->from(PropertyCname::class, 'cname')
            ->join(
                Property::class, 'property', Join::WITH, $qb->expr()->andX(
                    $qb->expr()->eq('property.name', 'cname.name'),
                    $qb->expr()->eq('property.value', 'cname.value')
                )
            )
            ->where(
                $qb->expr()->in('property.id', $propertiesIds)
            );


        $sql = $qb->getQuery()->getSQL();
        $result = $this->getEntityManager()->getConnection()->executeQuery($sql)
            ->fetchAllAssociative();

        $synonyms = [];
        foreach ($result as $key => $row) {
            $row = $this->clearKeysRow($row);
            $synonyms[$row['property_name_id']][$row['property_value_id']] = $row['synonym'];
        }

        return $synonyms;
    }
}
