<?php


namespace App\Domain\Property\Repository;


use App\Domain\Property\Entity\PropertyName;
use App\Traits\QueryBuilderTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PropertyNameRepository extends ServiceEntityRepository
{
    use QueryBuilderTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyName::class);
    }

    public function findAllAssoc(): array
    {
        $sql = $this
            ->createQueryBuilder('property_name')
            ->select('partial property_name.{id, value}')
            ->getQuery()
            ->getSQL();

        $result = $this->getEntityManager()->getConnection()->fetchAllAssociative($sql);

        $total = count($result);
        $arr2return = [];

        for ($n = 0; $n < $total; $n++) {
            $key = mb_strtolower($result[$n]['value_1']);
            $key = str_replace('ё', 'е', $key);

            $arr2return[$key] = (int) $result[$n]['id_0'];
            unset($result[$n]);
        }

        return $arr2return;
    }
}
