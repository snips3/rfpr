<?php

namespace App\Domain\City\ParamConverter;

use App\Domain\City\Entity\City;
use App\Domain\City\Repository\CityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class CityParamConverter implements ParamConverterInterface
{
    private CityRepository $cityRepository;

    public function __construct(CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }
    public function apply(Request $request, ParamConverter $configuration)
    {
        $domain = $request->headers->get('user-city', 'msk');
        $city = $this->cityRepository->findOneBy(['domain' => $domain]);
        $request->attributes->set($configuration->getName(), $city);

        return true;
    }

    public function supports(ParamConverter $configuration)
    {
        return City::class === $configuration->getClass();
    }
}
