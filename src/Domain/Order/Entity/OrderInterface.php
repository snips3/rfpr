<?php

namespace App\Domain\Order\Entity;

use App\Domain\Customer\Entity\Customer;
use Doctrine\Common\Collections\Collection;

interface OrderInterface
{
    public function getId(): int;
    public function getCustomer(): Customer;
    public function getOrderItems(): Collection;
    public function getTotalPrice(): int;
}
