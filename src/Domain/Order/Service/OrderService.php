<?php

namespace App\Domain\Order\Service;

use App\Domain\CartItem\Service\CartItemService;
use App\Domain\Order\Entity\Order;
use App\Domain\Order\Entity\OrderItem;
use App\Domain\Product\Entity\Price;
use App\Domain\Product\Repository\PriceRepository;
use App\Domain\Shop\Entity\Shop;
use Doctrine\ORM\EntityManagerInterface;

class OrderService
{
    private EntityManagerInterface $entityManager;
    private CartItemService $cartItemService;

    public function __construct(EntityManagerInterface $entityManager, CartItemService $cartItemService)
    {
        $this->entityManager = $entityManager;
        $this->cartItemService = $cartItemService;
    }

    public function addOrder(Order $order)
    {
        $customer = $order->getCustomer();

        $cartItems = $customer->getCartItems();
        //TODO вынести в фабрику или в метод
        foreach ($cartItems as $cartItem) {
            $orderItem = new OrderItem();
            $orderItem->setOrder($order);
            $orderItem->setOffer($cartItem->getOffer());
            //TODO магазин
            $orderItem->setShop($cartItem->getOffer()->getOfferPrice()->getShop());
            $orderItem->setCount($cartItem->getCount());
            $orderItem->setPrice($cartItem->getOffer()->getOfferPrice()->getPriceActual());
            $order->addOrderItem($orderItem);
            $this->entityManager->persist($orderItem);
        }

        $this->cartItemService->deleteAll($customer);

        $this->entityManager->persist($customer);
        $this->entityManager->flush();
    }
}
