<?php

namespace App\Domain\Order\ParamConverter;

use App\Domain\Customer\Entity\Customer;
use App\Domain\Order\Entity\Order;
use App\Domain\Order\Entity\OrderItem;
use App\Domain\Product\Entity\Offer;
use App\Domain\Product\Entity\Price;
use App\Domain\Shop\Entity\Shop;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class OrderDTOParamConverter implements ParamConverterInterface
{
    private EntityManagerInterface $entityManager;
    private Security $security;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        $jsonData = json_decode($request->getContent(), true);

        $order = new Order();

        /** @var Customer $customer */
        $customer = $this->security->getUser();
        $order->setCustomer($customer);
        $customer->addOrder($order);

        $shop = $this->entityManager->getRepository(Shop::class)->find($jsonData['delivery']['shop']);

        $offerRepository = $this->entityManager->getRepository(Offer::class);
        foreach ($jsonData['items'] as $item) {
            $offer = $offerRepository->find($item['offer']);
            $orderItem = new OrderItem();
            $orderItem->setOrder($order);
            $orderItem->setShop($shop);
            $orderItem->setOffer($offer);
            $orderItem->setPrice($offer->getPrices()->filter(function (Price $price) use ($shop): bool {
                return $shop === $price->getShop();
            })->first()->getActual());
            $orderItem->setCount(abs(intval($item['count'])) ?? 1);

            $order->addOrderItem($orderItem);
            $this->entityManager->persist($orderItem);
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        $request->attributes->set($configuration->getName(), $order ?? null);
    }

    public function supports(ParamConverter $configuration)
    {
        return Order::class === $configuration->getClass();
    }
}
