<?php

namespace App\Domain\Order\ParamConverter;

use App\Domain\Order\Entity\Order;
use App\Domain\Order\Repository\OrderRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class OrderParamConverter implements ParamConverterInterface
{
    private OrderRepository $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }
    public function apply(Request $request, ParamConverter $configuration)
    {
        try {
            $jsonData = json_decode($request->getContent(), true);

            $offer = $this->orderRepository->find($jsonData['order_id']);

            $request->attributes->set($configuration->getName(), $offer);
        } catch (\Exception $exception){
            //TODO
        }
    }

    public function supports(ParamConverter $configuration)
    {
        return Order::class === $configuration->getClass();
    }
}
