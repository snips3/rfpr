<?php

namespace App\Domain\Category\Repository;

use App\Domain\Category\Collection\CategoryCollection;
use App\Domain\Category\Entity\Category;
use App\Domain\City\Entity\City;
use App\Domain\Product\Entity\Offer;
use App\Domain\Product\Entity\Price;
use App\Domain\Product\Entity\Product;
use App\Domain\Shop\Entity\Shop;
use App\Traits\QueryBuilderTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Collection\Collection;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    use QueryBuilderTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function findByCity(City $city): array
    {
        $shopsIds = $city->getShops()->getIds();

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('partial product.{id}')
            ->from(Price::class, 'price')
            ->join(
                Offer::class,
                'offer',
                Join::WITH,
                $qb->expr()->eq('price.offer', 'offer')
            )
            ->join(
                Product::class,
                'product',
                Join::WITH,
                $qb->expr()->eq('product', 'offer.product'),
            )
            ->where($qb->expr()->in('price.shop', $shopsIds))
            ->groupBy('product.category');

        $sql = $qb->getQuery()->getSQL();

        $result = $this->getEntityManager()
            ->getConnection()->executeQuery($sql)->fetchAllAssociative();

        $tree = $this->getTreeIds();
        $categoryIds = [];
        foreach ($result as $row) {
            $row = $this->clearKeysRow($row);
            if (!empty($tree[$row['category_id']])) {
                $categoryIds[] = $row['category_id'];
                $parent = $tree[$row['category_id']][1];
                while ($parent) {
                    $categoryIds[] = $parent;
                    $parent = $tree[$parent][1] ?? null;
                }
            }
        }

        return $this->findBy(['id' => $categoryIds]);
    }

    public function getTreeIds(): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('partial category.{id}')
            ->from(Category::class, 'category')
            ->where($qb->expr()->isNotNull('category.active'));

        $sql = $qb->getQuery()->getSQL();
        $result = $this->getEntityManager()
            ->getConnection()->executeQuery($sql)->fetchAllNumeric();

        $tree = [];
        foreach ($result as $row) {
            $tree[$row[0]] = $row;
        }

        return $tree;
    }

    public function findByIds(array $ids): CategoryCollection
    {
        sort($ids);
        $qb = $this->createQueryBuilder('category');

        $qb->where($qb->expr()->in('category.id', $ids));

        return new CategoryCollection($qb->getQuery()->getResult());
    }
}
