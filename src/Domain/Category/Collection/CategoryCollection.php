<?php

namespace App\Domain\Category\Collection;

use App\Domain\Category\Entity\Category;
use App\Domain\Product\Collection\ProductCollection;
use Doctrine\Common\Collections\ArrayCollection;

class CategoryCollection extends ArrayCollection
{
    public function getProducts(): ProductCollection
    {
        $products = [];
        $this->map(function (Category $category) use (&$products): void {
            $products = array_merge($products, $category->getProducts()->toArray());
            $products = array_merge($products, $category->getChildren()->getProducts()->toArray());
        });

        return new ProductCollection($products);
    }

    public function getIds(bool $deep = true): array
    {
        $ids = [];
        $this->map(function (Category $category) use (&$ids, $deep): void {
            $ids[] = $category->getId();
            if ($deep) {
                $ids = array_merge(
                    $ids,
                    $category->getChildren()->getIds($deep)
                );
            }
        });

        return $ids;
    }
}
