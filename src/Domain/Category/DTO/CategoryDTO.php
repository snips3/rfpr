<?php

namespace App\Domain\Category\DTO;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CategoryDTO
{
    /**
     * @Assert\NotBlank
     * @Assert\Uuid
     */
    public string $uuid;
    /**
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    public string $name;
    /**
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    public string $slug;
    /**
     * @Assert\NotBlank
     * @Assert\Type(
     * type="bool",
     * message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public bool $active = true;
}
