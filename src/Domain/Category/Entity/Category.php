<?php

namespace App\Domain\Category\Entity;

use App\Domain\Category\Repository\CategoryRepository;
use App\Domain\Category\Collection\CategoryCollection;
use App\Domain\Product\Collection\ProductCollection;
use App\Domain\Product\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 * @ORM\Table(name="category", uniqueConstraints={@ORM\UniqueConstraint(columns={"uuid"})})
 */
class Category implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $slug;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Category\Entity\Category", inversedBy="children")
     */
    private ?Category $parent;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Category\Entity\Category", mappedBy="parent")
     */
    private Collection $children;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="category", orphanRemoval=true)
     */
    private Collection $products;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Category\Entity\Category")
     * @ORM\JoinTable(name="category_related",
     *      joinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="related_id", referencedColumnName="id")}
     *      )
     */
    private Collection $related;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $active = true;

    private function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
        $this->children = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->related = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getIds(): array
    {
        $ids = array_merge([$this->id], $this->getChildren()->getIds());

        sort($ids);

        $ids = array_unique($ids);

        return $ids;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return (string) ($this->slug ?? $this->id);
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function getRoot(): ?self
    {
        $parent = $this;
        while ($parent->getParent()) {
            $parent = $parent->getParent();
        }

        return $parent;
    }

    public function getChildren(): CategoryCollection
    {
        return new CategoryCollection($this->children->toArray());
    }

    public function getRelated(): CategoryCollection
    {
        return new CategoryCollection($this->related->toArray());
    }

    public function getProducts(): ProductCollection
    {
        $products = array_merge(
            $this->products->toArray(),
            $this->getChildren()->getProducts()->toArray()
        );

        return new ProductCollection($products);
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function isActive(): bool
    {
        return (bool) $this->active;
    }

    public function getParentId(): ?int
    {
        if ($this->getParent()) {
            return  $this->getParent()->getId();
        }

        return null;
    }

    public function getUrl(): string
    {
        $parts = [$this->getSlug()];
        if ($this->parent) {
            array_unshift($parts, $this->getParent()->getUrl());
        } else {
            array_unshift($parts, '/catalog');
        }

        return join('/', $parts);
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'parent' => $this->getParentId(),
            'name' => $this->getName(),
            'slug' => $this->getSlug(),
        ];
    }
}
