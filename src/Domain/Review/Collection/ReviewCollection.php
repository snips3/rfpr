<?php

namespace App\Domain\Review\Collection;

use App\Domain\Review\Entity\Review;
use Doctrine\Common\Collections\ArrayCollection;

class ReviewCollection extends ArrayCollection implements \JsonSerializable
{
    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
