<?php

namespace App\Domain\Customer\Repository;

use App\Domain\Customer\DTO\CustomerRegistrationDTO;
use App\Domain\Customer\Entity\Customer;
use App\Domain\Customer\Service\RegistrationService;
use App\Utility\Phone\Phone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Customer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customer[]    findAll()
 * @method Customer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Customer::class);
    }

    public function findOneByPhone(Phone $phone): ?Customer
    {
        return $this->findOneBy(['phone' => $phone->getValue()]);
    }
}
