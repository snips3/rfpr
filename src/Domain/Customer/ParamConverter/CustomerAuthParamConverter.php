<?php

namespace App\Domain\Customer\ParamConverter;

use App\Domain\Customer\DTO\CustomerRegistrationDTO;
use App\Domain\Customer\Entity\Customer;
use App\Domain\Customer\Repository\CustomerRepository;
use App\Domain\Customer\Service\RegistrationService;
use App\Domain\Sms\Repository\SmsCodeRepository;
use App\Utility\Phone\Phone;
use App\Utility\Phone\PhoneException;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use MongoDB\Driver\Exception\AuthenticationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class CustomerAuthParamConverter implements ParamConverterInterface
{
    private EntityManagerInterface $entityManager;
    private SmsCodeRepository $smsRepository;
    private CustomerRepository $customerRepository;

    public function __construct(EntityManagerInterface $entityManager, CustomerRepository $customerRepository, SmsCodeRepository $smsRepository)
    {
        $this->smsRepository = $smsRepository;
        $this->entityManager = $entityManager;
        $this->customerRepository = $customerRepository;
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        try {
            $jsonData = json_decode($request->getContent(), true);
            $phone = new Phone($jsonData['phone']);

            $customer = $this->customerRepository->findOneByPhone($phone);

            $sms = $this->smsRepository->getActiveForPhone($customer->getPhone());

            if ($sms && $sms->getValue() === $jsonData['sms']) {
                $sms->setActive(false);
                $this->entityManager->flush();

                $request->attributes->set($configuration->getName(), $customer);
            } else {
                throw new \Exception('Bad Credentials', 500);
            }
        } catch (\Exception $exception) {
            //TODO что-то получше
            throw new \Exception('Bad Credentials', 500);
        }
    }

    public function supports(ParamConverter $configuration): bool
    {
        return Customer::class === $configuration->getClass();
    }
}
