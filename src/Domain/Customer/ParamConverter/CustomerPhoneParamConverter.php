<?php

namespace App\Domain\Customer\ParamConverter;

use App\Domain\Customer\Entity\Customer;
use App\Domain\Customer\Repository\CustomerRepository;
use App\Utility\Phone\Phone;
use App\Utility\Phone\PhoneException;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class CustomerPhoneParamConverter implements ParamConverterInterface
{
    private CustomerRepository $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param Request        $request
     * @param ParamConverter $configuration
     *
     * @return bool
     * @throws PhoneException
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $jsonData = json_decode($request->getContent(), true);
        try {
            $phone = new Phone(($jsonData['phone'] ?? $jsonData['username']));
        } catch (Exception $exception) {
            throw new PhoneException();
        }

        $customer = $this->customerRepository->findOneByPhone($phone);

        $request->attributes->set($configuration->getName(), ($customer ?? null));
        $request->attributes->set("phone", $phone);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getName() === 'customer' && Customer::class === $configuration->getClass();
    }
}
