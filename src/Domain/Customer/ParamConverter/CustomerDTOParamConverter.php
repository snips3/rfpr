<?php

namespace App\Domain\Customer\ParamConverter;

use App\Domain\Customer\DTO\CustomerRegistrationDTO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class CustomerDTOParamConverter implements ParamConverterInterface
{
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        $jsonData = json_decode($request->getContent(), true);

        $customerDTO = new CustomerRegistrationDTO($jsonData);

        $request->attributes->set($configuration->getName(), $customerDTO ?? null);
    }

    public function supports(ParamConverter $configuration)
    {
        return CustomerRegistrationDTO::class === $configuration->getClass();
    }
}
