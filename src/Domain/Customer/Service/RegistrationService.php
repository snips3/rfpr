<?php

namespace App\Domain\Customer\Service;

use App\Domain\Customer\DTO\CustomerRegistrationDTO;
use App\Domain\Customer\Entity\Customer;
use App\Domain\Customer\Exception\CustomerRegistrationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationService
{
    private ValidatorInterface $validator;
    private UserPasswordEncoderInterface $passwordEncoder;
    private EntityManagerInterface $entityManager;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager
    ) {
        $this->validator = $validator;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    public function register(CustomerRegistrationDTO $customerDTO): Customer
    {
        $errors = $this->validator->validate($customerDTO);

        if ($errors->count()) {
            throw CustomerRegistrationException::fromValidatorErrors($errors);
        }
        $customer = Customer::createFromRegistrationDTO($customerDTO);
        if ($customerDTO->password) {
            $customer->setPassword(
                $this->passwordEncoder->encodePassword($customer, $customerDTO->password)
            );
        }

        $this->entityManager->persist($customer);
        $this->entityManager->flush();

        return $customer;
    }
}
