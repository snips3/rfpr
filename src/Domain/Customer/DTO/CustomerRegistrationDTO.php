<?php

namespace App\Domain\Customer\DTO;

use App\Utility\DTO;
use App\Utility\Phone\Phone;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

class CustomerRegistrationDTO extends DTO
{
    /**
     * @Assert\Type(
     *     type="App\Utility\Phone\Phone",
     * )
     */
    public ?Phone $phone = null;

    /**
     * @Assert\Email()
     */
    public ?string $email = null;

    public ?string $password = null;

    public ?string $name = null;
}
