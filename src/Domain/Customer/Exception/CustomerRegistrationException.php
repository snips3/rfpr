<?php

namespace App\Domain\Customer\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

final class CustomerRegistrationException extends \Exception
{
    public $message = 'user.registration.failed';

    private $constraintViolationList;

    public function getStatusCode()
    {
        return Response::HTTP_BAD_REQUEST;
    }

    public static function fromValidatorErrors(ConstraintViolationListInterface $constraintViolationList)
    {
        $exception = new static();
        $exception->setConstraintViolationList($constraintViolationList);

        return $exception;
    }

    private function setConstraintViolationList(ConstraintViolationListInterface $constraintViolationList): self
    {
        $this->constraintViolationList = $constraintViolationList;

        return $this;
    }

    public function getDetails(): array
    {
        $details = [];

        if ($this->constraintViolationList instanceof ConstraintViolationListInterface) {
            foreach ($this->constraintViolationList as $error) {
                $details[] = [
                    'property' => $error->getPropertyPath(),
                    'message' => $error->getMessage(),
                ];
            }
        }

        return $details;
    }
}
