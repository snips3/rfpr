<?php

namespace App\Domain\Customer\Entity;

use App\Domain\Customer\DTO\CustomerRegistrationDTO;
use App\Domain\Product\Collection\ProductCollection;
use App\Domain\CartItem\Collection\CartItemCollection;
use App\Domain\Customer\Collection\CustomerCollection;
use App\Domain\Product\Entity\Product;
use App\Domain\Customer\Repository\CustomerRepository;
use App\Domain\Customer\Entity\Information;
use App\Domain\Sms\Entity\Sms;
use App\Utility\Phone\Phone;
use App\Domain\CartItem\Entity\CartItem;
use App\Domain\Order\Entity\Order;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=CustomerRepository::class)
 * @UniqueEntity("phone")
 * @UniqueEntity("email")
 */
class Customer implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="phone", length=255, nullable=true, unique=true)
     */
    private ?Phone $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private ?string $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Product\Entity\Product")
     * @ORM\JoinTable(name="customer_favorites",
     *      joinColumns={@ORM\JoinColumn(name="customer_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="favorite_product_id", referencedColumnName="id")}
     * )
     */
    private Collection $favorites;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Product\Entity\Product")
     * @ORM\JoinTable(name="customer_comparisons",
     *      joinColumns={@ORM\JoinColumn(name="customer_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="comparison_product_id", referencedColumnName="id")}
     * )
     */
    private Collection $comparisons;

    /**
     * @ORM\OneToMany(targetEntity=CartItem::class, mappedBy="customer", orphanRemoval=true)
     */
    private Collection $cartItems;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="customer", orphanRemoval=true)
     */
    private Collection $orders;

    public function __construct()
    {
        $this->favorites = new ArrayCollection();
        $this->comparisons = new ArrayCollection();
        $this->sms = new ArrayCollection();
        $this->cartItems = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public static function createFromRegistrationDTO(CustomerRegistrationDTO $customerRegistration): self
    {
        $customer = new static();
        $customer
            ->setPhone($customerRegistration->phone)
            ->setEmail($customerRegistration->email)
            ->setName($customerRegistration->name);

        return $customer;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPhone(): ?Phone
    {
        return $this->phone;
    }

    public function setPhone(?Phone $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFavorites(): Collection
    {
        return new ProductCollection($this->favorites->toArray());
    }

    public function addFavorite(Product $favorite): self
    {
        if (!$this->favorites->contains($favorite)) {
            $this->favorites->add($favorite);
        }

        return $this;
    }

    public function removeFavorite(Product $favorite): self
    {
        if ($this->favorites->contains($favorite)) {
            $this->favorites->removeElement($favorite);
            // set the owning side to null (unless already changed)
        }

        return $this;
    }

    public function getComparisons(): Collection
    {
        return new ProductCollection($this->comparisons->toArray());
    }

    public function addComparison(Product $comparison): self
    {
        if (!$this->comparisons->contains($comparison)) {
            $this->comparisons->add($comparison);
        }

        return $this;
    }

    public function removeComparison(Product $comparison): self
    {
        if ($this->comparisons->contains($comparison)) {
            $this->comparisons->removeElement($comparison);
        }

        return $this;
    }

    public function getSms(): Collection
    {
        return $this->sms;
    }

    public function getActiveSms(): ?Sms
    {
        $sms = $this->getSms()->last();

        return $sms->isActive() ? $sms : null;
    }

    public function getCartItems(): Collection
    {
        return new CartItemCollection($this->cartItems->toArray());
    }

    public function addCartItem(CartItem $cartItem): self
    {
        if (!$this->cartItems->contains($cartItem)) {
            $this->cartItems->add($cartItem);
            $cartItem->setCustomer($this);
        }

        return $this;
    }

    public function removeCartItem(CartItem $cartItem): self
    {
        $this->cartItems->removeElement($cartItem);

        return $this;
    }

    public function removeCartItems(): self
    {
        $this->cartItems->clear();

        return $this;
    }

    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders->add($order);
            $order->setCustomer($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getCustomer() === $this) {
                $order->setCustomer(null);
            }
        }

        return $this;
    }

    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function getSalt()
    {
    }

    public function getUsername()
    {
        return $this->getPhone()->getValue();
    }

    public function eraseCredentials()
    {
        return $this;
    }
}
