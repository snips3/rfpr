<?php

namespace App\Domain\Shop\Collection;

use App\Domain\Shop\Entity\Shop;
use Doctrine\Common\Collections\ArrayCollection;
use JsonSerializable;

class ShopCollection extends ArrayCollection implements JsonSerializable
{
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function getIds(): array
    {
        $ids = $this->map(function (Shop $shop): int {
            return $shop->getId();
        })->toArray();
        sort($ids);

        return $ids;
    }
}
