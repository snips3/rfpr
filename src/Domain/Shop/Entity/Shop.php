<?php

namespace App\Domain\Shop\Entity;

use App\Domain\City\Entity\City;
use App\Domain\Shop\DTO\ShopDTO;
use App\Domain\Shop\Repository\ShopRepository;
use App\Domain\Product\Entity\Offer;
use App\Utility\Phone\Phone;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=ShopRepository::class)
 * @UniqueEntity("uuid")
 */
class Shop implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="shops")
     */
    private ?City $city = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $postalCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $geo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $contacts;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $schedule;

    private function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
        $this->offers = new ArrayCollection();
    }

    public static function fromUuid(UuidInterface $uuid)
    {
        $shop = new self($uuid);

        return $shop;
    }

    public static function createFromDTO(ShopDTO $shopDTO): Shop
    {
        $shop = new static(Uuid::fromString($shopDTO->uuid));

        $shop->setName($shopDTO->name)
            ->setAddress($shopDTO->address)
            ->setContacts($shopDTO->contacts)
            ->setGeo($shopDTO->geo)
            ->setPostalCode($shopDTO->postal_code)
            ->setSchedule($shopDTO->schedule);

        return $shop;
    }

    public function updateFromDTO(ShopDTO $shopDTO): Shop
    {
        $this->setName($shopDTO->name)
            ->setAddress($shopDTO->address)
            ->setContacts($shopDTO->contacts)
            ->setPostalCode($shopDTO->postal_code)
            ->setSchedule($shopDTO->schedule)
            ->setGeo($shopDTO->geo);

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPostalCode(): ?int
    {
        return $this->postalCode;
    }

    public function setPostalCode(?int $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getGeo(): ?string
    {
        return $this->geo;
    }

    public function setGeo(?string $geo): self
    {
        $this->geo = $geo;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getContacts(): string
    {
        return $this->contacts;
    }

    public function setContacts(string $contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    public function getSchedule(): string
    {
        return $this->schedule;
    }

    public function setSchedule(string $schedule): self
    {
        $this->schedule = $schedule;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'geo' => $this->getGeo(),
            'name' => $this->getName(),
            'city' => $this->getCity() ? $this->getCity()->getId() : null,
            'address' => $this->getAddress(),
            'postalCode' => $this->getPostalCode(),
            'image' => null,
        ];
    }
}
