<?php

namespace App\Domain\Shop\DTO;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ShopDTO
{
    /**
     * @Assert\NotBlank
     * @Assert\Uuid
     */
    public string $uuid;

    /**
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    public string $name;
    /**
     * @Assert\Length(max = 255)
     */
    public ?string $address = null;
    /**
     * @Assert\Length(max = 255)
     */
    public ?int $postal_code = null;
    /**
     * @Assert\Length(max = 255)
     */
    public ?string $schedule = null;
    /**
     * @Assert\Length(max = 255)
     */
    public ?string $contacts = null;
    /**
     * @Assert\Length(max = 255)
     */
    public ?string $geo = null;
}
