<?php

namespace App\Domain\Product\Entity;

use App\Domain\Product\DTO\PriceDTO;
use App\Domain\Shop\Entity\Shop;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @UniqueEntity("uuid")
 * @ORM\Table(
 *     indexes={@ORM\Index(
 *          name="prices",
 *          columns={"shop_id", "offer_id", "quantity", "actual", "old"}
 *     )},
 *     uniqueConstraints={@ORM\UniqueConstraint(columns={"shop_id", "offer_id"})}
 *     )
 */
class Price implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Product\Entity\Offer", inversedBy="prices")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private Offer $offer;

    /**
     * @ORM\ManyToOne(targetEntity=Shop::class)
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private Shop $shop;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private int $actual;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $old;

    /**
     * @ORM\Column(type="integer")
     */
    private int $quantity;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private \DateTimeInterface $created_at;

    /**
     * @ORM\Column(type="datetime", columnDefinition="DATETIME on update CURRENT_TIMESTAMP", nullable=true, options={"default" : null})
     */
    private ?\DateTimeInterface $updated_at = null;

    public static function createFromDTO(PriceDTO $priceDTO)
    {
        $price = new self();

        $price->actual = $priceDTO->price_actual;
        $price->old = $priceDTO->price_actual;

        return $price;
    }

    public function updateFromDTO(PriceDTO $priceDTO)
    {
        $this->actual = $priceDTO->price_actual;
        $this->old = $priceDTO->price_actual;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getOffer(): Offer
    {
        return $this->offer;
    }

    public function getShop(): Shop
    {
        return $this->shop;
    }

    public function getActual(): int
    {
        return $this->actual;
    }

    public function getOld(): ?int
    {
        return $this->old;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function jsonSerialize(): array
    {
        return [
            'shop' => $this->getShop()->getId(),
            'actual' => $this->getActual(),
            'old' => $this->getOld(),
            'available' => (bool) $this->getQuantity(),
        ];
    }
}
