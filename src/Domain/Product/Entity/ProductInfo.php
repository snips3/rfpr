<?php

namespace App\Domain\Product\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="products_info")
 */
class ProductInfo
{
    /**
     * @ORM\Id()
     * @ORM\OneToOne(targetEntity="App\Domain\Product\Entity\Product", inversedBy="info")
     */
    private Product $product;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $nameYandexMarket;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $article;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $dimensions;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?int $weight;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $color;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNameYandexMarket(): ?string
    {
        return $this->nameYandexMarket;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function getArticle(): ?string
    {
        return $this->article;
    }

    public function getDimensions(): ?string
    {
        return $this->dimensions;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }
}
