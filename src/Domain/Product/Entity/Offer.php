<?php

namespace App\Domain\Product\Entity;

use App\Domain\Order\DTO\OrderDTO;
use App\Domain\Product\DTO\OfferDTO;
use App\Domain\Property\Collection\PropertyCollection;
use App\Domain\Property\Entity\Property;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Domain\Product\Repository\OfferRepository")
 * @UniqueEntity("uuid")
 */
class Offer implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Product\Entity\Product", inversedBy="offers")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private Product $product;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $barCode = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $name = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Product\Entity\Price", mappedBy="offer", cascade={"persist", "remove"}, fetch="EAGER")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private Collection $prices;

    /**
     * @ORM\OneToMany (targetEntity="App\Domain\Property\Entity\Property", mappedBy="offer", cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private Collection $properties;

    public function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
        $this->properties = new ArrayCollection();
    }

    public static function fromUuid(UuidInterface $uuid)
    {
        $offer = new self($uuid);

        return $offer;
    }

    public static function createFromDTO(OfferDTO $offerDTO)
    {
        $offer = new static(Uuid::fromString($offerDTO->uuid));

        $offer
            ->setBarCode($offerDTO->barCode)
        ;

        return $offer;
    }

    public function getBarCode(): ?string
    {
        return $this->barCode;
    }

    public function setBarCode(?string $barCode): Offer
    {
        $this->barCode = $barCode;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getProperties(): PropertyCollection
    {
        return new PropertyCollection($this->properties->toArray());
    }

    public function addProperty(Property $property): self
    {
        if (!$this->properties->contains($property)) {
            $this->properties->add($property);
            $property->setOffer($this);
        }

        return $this;
    }

    public function removeProperty(Property $property): self
    {
        if ($this->properties->contains($property)) {
            $this->properties->removeElement($property);
        }

        return $this;
    }

    public function getPrices(): Collection
    {
        return $this->prices;
    }

    public function jsonSerialize(): array
    {
        return array_filter([
            'id' => $this->getId(),
            'name' => $this->name,
            // 'quantity' => $this->getOfferPrice()->getQuantity(),
            'prices' => $this->getPrices()->toArray(),
            'properties' => $this->getProperties(),
        ]);
    }
}
