<?php

namespace App\Domain\Product\Entity;

use App\Domain\Category\Entity\Category;
use App\Domain\Product\Collection\OfferCollection;
use App\Domain\Property\Collection\PropertyCollection;
use App\Domain\Property\Entity\Property;
use App\Domain\Review\Entity\Review;
use App\Domain\Product\Collection\ProductCollection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="App\Domain\Product\Repository\ProductRepository")
 * @ORM\Table(name="products", uniqueConstraints={@ORM\UniqueConstraint(columns={"uuid"})})
 */
class Product implements CityAware, JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    private UuidInterface $uuid;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private ?string $code;

    /**
     * @ORM\OneToOne(targetEntity="App\Domain\Product\Entity\ProductInfo", mappedBy="product")
     */
    private ProductInfo $info;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    private Category $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Product\Entity\Offer",mappedBy="product", orphanRemoval=true)
     */
    private Collection $offers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Product\Entity\Product")
     * @ORM\JoinTable(name="products_colors",
     *      joinColumns={@ORM\JoinColumn(name="id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")}
     *      )
     */
    private Collection $colors;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Product\Entity\Product")
     * @ORM\JoinTable(name="products_related",
     *      joinColumns={@ORM\JoinColumn(name="id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")}
     *      )
     */
    private Collection $related;

    /**
     * @ORM\OneToMany (targetEntity="App\Domain\Property\Entity\Property", mappedBy="product", cascade={"persist"})
     */
    private Collection $properties;

    /**
     * @ORM\OneToMany(targetEntity=Review::class, mappedBy="product", orphanRemoval=true)
     */
    private Collection $reviews;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default": 1})
     */
    private ?bool $active = true;

    private function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
        $this->info = new ProductInfo($this);
        $this->offers = new ArrayCollection();
        $this->colors = new ArrayCollection();
        $this->related = new ArrayCollection();
        $this->properties = new ArrayCollection();
        $this->reviews = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function getProperties(): PropertyCollection
    {
        return new PropertyCollection($this->properties->toArray());
    }

    public function addProperty(Property $property): self
    {
        if (!$this->properties->contains($property)) {
            $this->properties[] = $property;
            $property->setProduct($this);
        }

        return $this;
    }

    public function removeProperty(Property $property): self
    {
        if ($this->properties->contains($property)) {
            $this->properties->removeElement($property);
        }

        return $this;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function isActive(): bool
    {
        return (bool) $this->active;
    }

    public function getRelated(): ProductCollection
    {
        return new ProductCollection($this->related->toArray());
    }

    public function getColors()
    {
        return new ProductCollection($this->colors->toArray());
    }

    public function getReviews()
    {
        return $this->reviews;
    }

    public function getOffers(): OfferCollection
    {
        return new OfferCollection($this->offers->toArray());
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers->add($offer);
        }
        $offer->setProduct($this);

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getInfo(): ProductInfo
    {
        return $this->info;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'category' => $this->getCategory()->getId(),
            'name' => $this->getInfo()->getName(),
            'description' => $this->getInfo()->getDescription(),
            'slug' => $this->getInfo()->getSlug(),
            // 'properties' => $this->getProperties()->getAssoc(),
            // 'offers' => $this->getOffers(),
            // 'related' => $this->getCategory()->getRelated()->getProducts($criteria)
        ];
    }
}
