<?php

namespace App\Domain\Product\Repository;

use App\Domain\Product\Collection\ProductCollection;
use App\Domain\Product\Entity\Product;
use App\Domain\Product\Entity\ProductInfo;
use App\Traits\QueryBuilderTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 */
class ProductRepository extends ServiceEntityRepository
{
    use QueryBuilderTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findBy(
        array $criteria,
        array $orderBy = null,
        $limit = null,
        $offset = null
    ): ProductCollection {
        return new ProductCollection(
            parent::findBy(
                $criteria,
                $orderBy,
                $limit,
                $offset
            )
        );
    }

    public function findProductsByIds(array $ids): array
    {
        sort($ids);
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('product, info')
            ->from(Product::class, 'product')
            ->join(ProductInfo::class, 'info', Join::WITH, 'info.product = product')
            ->where($qb->expr()->in('product.id', $ids));

        $sql = $qb->getQuery()->getSQL();

        $result = $this->getEntityManager()
            ->getConnection()->executeQuery($sql)->fetchAllAssociative();
        $result = $this->clearKeys($result);

        $products = [];
        foreach ($result as $row) {
            $products[$row['id']] = $row;
        }

        return $products;
    }
}
