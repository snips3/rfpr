<?php

namespace App\Domain\Product\DTO;
use Symfony\Component\Validator\Constraints as Assert;

class OfferDTO
{
    private const MAPPING = [
        'id' => 'uuid',
        'barcode' => 'barCode',
    ];

    /**
     * @Assert\NotBlank
     * @Assert\Uuid
     */
    public string $uuid;

    /**
     * @Assert\NotBlank
     */
    public string $barCode;

    public function __construct(array $data)
    {
        foreach (['id', 'barcode'] as $prop) {
            if (array_key_exists($prop, $data)) {
                $mapped = strtr($prop, self::MAPPING);
                $this->{$mapped} = $data[$prop];
            }
        }
    }
}
