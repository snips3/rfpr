<?php

namespace App\Domain\Product\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class ProductDTO
{
    private const MAPPING = [
        'id' => 'uuid',
        'desc' => 'description',
    ];

    /**
     * @Assert\NotBlank
     * @Assert\Uuid
     */
    public string $uuid;

    /**
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    public string $name;

    /**
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    public string $slug;

    /**
     * @Assert\NotBlank
     * @Assert\Type(
     *     type="bool",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public bool $active = true;

    /**
     * @Assert\Length(max = 65535)
     */
    public ?string $description = null;

    /**
     * @Assert\Length(max = 32)
     */
    public ?string $article = null;

    /**
     * @Assert\Length(max = 32)
     */
    public ?string $code = null;

    /**
     * @Assert\Length(max = 32)
     */
    public ?string $image = null;

    public function __construct(array $data)
    {
        foreach (['id', 'name', 'article', 'desc'] as $prop) {
            $mapped = strtr($prop, self::MAPPING);
            $this->{$mapped} = $data[$prop];
        }
    }
}
