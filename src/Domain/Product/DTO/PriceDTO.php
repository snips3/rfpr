<?php

namespace App\Domain\Product\DTO;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PriceDTO
{
    /**
     * @Assert\NotBlank
     * @Assert\GreaterThan(0)
     */
    public int $price_actual;
    /**
     * @Assert\GreaterThan(0)
     */
    public ?int $price_old = null;
}
