<?php

namespace App\Domain\Product\ParamConverter;

use App\Domain\Product\Entity\Offer;
use App\Domain\Product\Repository\OfferRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class OfferParamConverter implements ParamConverterInterface
{
    private OfferRepository $offerRepository;

    public function __construct(OfferRepository $offerRepository)
    {
        $this->offerRepository = $offerRepository;
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        try {
            $jsonData = json_decode($request->getContent(), true);

            $offer = $this->offerRepository->find($jsonData['offer_id']);

            $request->attributes->set($configuration->getName(), $offer);
        } catch (\Exception $exception){
            //TODO
        }
    }

    public function supports(ParamConverter $configuration)
    {
        return Offer::class === $configuration->getClass();
    }
}

