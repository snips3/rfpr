<?php

namespace App\Domain\Product\Collection;

use App\Domain\Product\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;

class ProductCollection extends ArrayCollection implements \JsonSerializable
{
    public function sortByPrice(): self
    {
        $iterator = $this->getIterator();

        $iterator->uasort(
            function (Product $a, Product $b) {
                return $a->getPrice() < $b->getPrice() ? -1 : 1;
            }
        );

        return new self(iterator_to_array($iterator));
    }

    public function getIds(): array
    {
        return $this->map(
            function (Product $product): int {
                return $product->getId();
            }
        )->toArray();
    }

    public function getMinMaxPrices(): array
    {
        $prices = $this->map(function (Product $product): int {
            return $product->getPrice();
        })->toArray();

        if (empty($prices)) {
            return [
                0,
                0,
            ];
        }

        return [
            min($prices),
            max($prices),
        ];
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
