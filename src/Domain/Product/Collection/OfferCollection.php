<?php

namespace App\Domain\Product\Collection;

use Doctrine\Common\Collections\ArrayCollection;

class OfferCollection extends ArrayCollection implements \JsonSerializable
{
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
