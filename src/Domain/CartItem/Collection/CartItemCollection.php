<?php

namespace App\Domain\CartItem\Collection;

use App\Domain\CartItem\Entity\CartItem;
use Doctrine\Common\Collections\ArrayCollection;

class CartItemCollection extends ArrayCollection implements \JsonSerializable
{
    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
