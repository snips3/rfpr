<?php

namespace App\Domain\CartItem\Service;

use App\Domain\CartItem\Entity\CartItem;
use App\Domain\Customer\Entity\Customer;
use App\Domain\Product\Entity\Offer;
use App\Domain\Product\Repository\OfferRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class CartItemService
{
    private EntityManagerInterface $entityManager;
    private OfferRepository $offerRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        OfferRepository $offerRepository
    ) {
        $this->entityManager = $entityManager;
        $this->offerRepository = $offerRepository;
    }

    public function add(Customer $customer, Offer $offer): void
    {
        $cartItem = new CartItem();
        $cartItem->setOffer($offer);
        $customer->addCartItem($cartItem);

        $this->entityManager->persist($cartItem);
        $this->entityManager->persist($customer);
        $this->entityManager->flush();
    }

    public function editCount(Customer $customer, CartItem $cartItem, int $count): void
    {
        if ($customer->getId() !== $cartItem->getCustomer()->getId()) {
            throw new Exception();
        }
        $cartItem->setCount($count);

        $this->entityManager->persist($cartItem);
        $this->entityManager->flush();
    }

    public function delete(Customer $customer, CartItem $cartItem): void
    {
        $customer->removeCartItem($cartItem);

        $this->entityManager->flush();
    }

    public function deleteAll(Customer $customer): void
    {
        $customer->removeCartItems();

        $this->entityManager->flush();
    }
}
