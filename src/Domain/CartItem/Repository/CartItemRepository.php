<?php

namespace App\Domain\CartItem\Repository;

use App\Domain\CartItem\Entity\CartItem;
use App\Domain\Customer\Entity\Customer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

/**
 * @method CartItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method CartItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method CartItem[]    findAll()
 * @method CartItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartItemRepository extends ServiceEntityRepository
{
    private Security $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, CartItem::class);

        $this->security = $security;
    }

    public function findByCartItem(int $offer_id): ?CartItem
    {
        return $this->findOneBy(['offer' => $offer_id, 'customer' => $this->security->getUser()]);
    }
}
