<?php

namespace App\Domain\CartItem\Entity;

use App\Domain\Customer\Entity\Customer;
use App\Domain\Product\Entity\Offer;
use App\Domain\CartItem\Repository\CartItemRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass=CartItemRepository::class)
 */
class CartItem implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Offer::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private Offer $offer;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="cartItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private Customer $customer;

    /**
     * @ORM\Column(type="integer")
     */
    private int $count = 1;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")`
     */
    private DateTimeInterface $createdAt;

    public function getId(): int
    {
        return $this->id;
    }

    public function getOffer(): Offer
    {
        return $this->offer;
    }

    public function setOffer(Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function setCustomer(Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'product' => $this->getOffer()->getProduct()->getId(),
            'offer' => $this->getOffer()->getId(),
            'count' => $this->getCount(),
        ];
    }
}
