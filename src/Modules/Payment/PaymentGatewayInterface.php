<?php

namespace App\Modules\Payment;

use App\Domain\Order\Entity\OrderInterface;

interface PaymentGatewayInterface
{
//    public function gateway(string $method, array $data);
    public function register(OrderInterface $order): array;
    public function getOrderStatus(OrderInterface $order): array;
}
