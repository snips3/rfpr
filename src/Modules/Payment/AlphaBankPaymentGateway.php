<?php

namespace App\Modules\Payment;

use App\Domain\Order\Entity\OrderInterface;
use Exception;

class AlphaBankPaymentGateway implements PaymentGatewayInterface
{
    private string $username;
    private string $password;
    private string $gatewayUrl;
    private string $returnUrl;

    public function __construct(string $username, string $password, string $gatewayUrl, string $returnUrl)
    {
        $this->username = $username;
        $this->password = $password;
        $this->gatewayUrl = $gatewayUrl;
        $this->returnUrl = $returnUrl;
    }

    public function register(OrderInterface $order): array
    {
        $data = $this->createDataForRegister($order);
        $response = $this->gateway('register.do', $data);
        if (isset($response['errorCode'])) {
            throw new Exception($response['errorMessage'], $response['errorCode']);
        }
        return $response;
    }

    public function getOrderStatus(OrderInterface $order): array
    {
        $data = $this->createDataForStatus($order);
        $response = $this->gateway('getOrderStatusExtended.do', $data);
        if (isset($response['errorCode'])) {
            throw new Exception($response['errorMessage'], $response['errorCode']);
        }
        return $response;
    }

    private function createDataForRegister(OrderInterface $order): array
    {
        return [
            'userName' => $this->username,
            'password' => $this->password,
            'orderNumber' => $order->getId(),
            'amount' => ($order->getTotalPrice() * 100),
            'returnUrl' => $this->returnUrl,
        ];
    }

    private function createDataForStatus(OrderInterface $order): array
    {
        return [
            'userName' => $this->username,
            'password' => $this->password,
            'orderNumber' => $order->getId(),
        ];
    }

    private function gateway(string $method, array $data)
    {
        $curl = curl_init();
        curl_setopt_array(
            $curl,
            [
                CURLOPT_URL => $this->gatewayUrl . $method,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => http_build_query($data)
            ]
        );
        $response = curl_exec($curl);

        $response = json_decode($response, true);
        curl_close($curl);

        return $response;
    }
}
