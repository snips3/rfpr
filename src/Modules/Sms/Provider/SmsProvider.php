<?php

namespace App\Modules\Sms\Provider;

use App\Domain\Sms\Entity\SmsCode;
use App\Domain\Sms\Entity\SmsInterface;
use App\Utility\Phone\Phone;

class SmsProvider implements SmsProviderInterface
{
    private string $gatewayUrl;
    private string $username;
    private string $password;

    public function __construct(string $gatewayUrl, string $username, string $password)
    {
        $this->gatewayUrl = $gatewayUrl;
        $this->username = $username;
        $this->password = $password;
    }

    public function send(SmsInterface $sms)
    {
        $url = $this->gatewayUrl . '?' . http_build_query([
            'login' => $this->username,
            'psw' => $this->password,
            'charset' => 'UTF-8',
            'phones' => $sms->getPhone()->getValue(),
//          'cost' => 0,
            'mes' => $sms->getValue(),
        ]);
        $ch = curl_init($url);
        curl_setopt_array(
            $ch,
            [
                CURLOPT_CONNECTTIMEOUT => 15,
                CURLOPT_TIMEOUT => 15,
                CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HEADER => true,
            ]
        );

        $response = curl_exec($ch);

        if ($response === false) {
            throw new \Exception('Невозможно подключиться к серверу', 500);
        }

        return $response;
    }
}
