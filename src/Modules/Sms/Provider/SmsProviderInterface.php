<?php

namespace App\Modules\Sms\Provider;

use App\Domain\Sms\Entity\Sms;
use App\Domain\Sms\Entity\SmsInterface;
use App\Utility\Phone\Phone;

interface SmsProviderInterface
{
    public function send(SmsInterface $sms);
}
