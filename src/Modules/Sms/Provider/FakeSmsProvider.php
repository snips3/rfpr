<?php

namespace App\Modules\Sms\Provider;

use App\Domain\Sms\Entity\SmsInterface;

class FakeSmsProvider implements SmsProviderInterface
{
    public function send(SmsInterface $sms)
    {
        return true;
    }
}